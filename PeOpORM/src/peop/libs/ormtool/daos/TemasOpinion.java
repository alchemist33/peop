/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.daos;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alquimista
 */
@Entity
@Table(name = "TemasOpinion")
public class TemasOpinion {

    @Id
    @Column(name = "IdTema")
    private UUID IdTema;
    @Column(name = "nombreTema")
    private String nombreTema;
    @Column(name = "categoriaTema")
    private String categoriaTema;

    public TemasOpinion() {
    }

    public TemasOpinion(UUID IdTema, String nombreTema, String categoriaTema) {
        this.IdTema = IdTema;
        this.nombreTema = nombreTema;
        this.categoriaTema = categoriaTema;
    }

    public UUID getIdTema() {
        return IdTema;
    }

    public void setIdTema(UUID IdTema) {
        this.IdTema = IdTema;
    }

    public String getNombreTema() {
        return nombreTema;
    }

    public void setNombreTema(String nombreTema) {
        this.nombreTema = nombreTema;
    }

    public String getCategoriaTema() {
        return categoriaTema;
    }

    public void setCategoriaTema(String categoriaTema) {
        this.categoriaTema = categoriaTema;
    }

}
