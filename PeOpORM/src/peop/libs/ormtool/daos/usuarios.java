/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.daos;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author alquimista
 */
@Entity
@Table(name = "usuarios")
public class usuarios {

    @Id
    @Column(name = "Id")
    private UUID Id;

    @Column(name = "Nombre")
    private String nombre;

    @Column(name = "Apellidos")
    private String apellidos;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "fechaNacimiento")
    private Date fechaNacimiento;

    @Column(name = "nacionalidad")
    private String nacionalidad;

    @Column(name = "paisResidencia")
    private String paisResidencia;

    @Column(name = "paisOrigen")
    private String paisOrigen;

    public usuarios() {
    }

    public usuarios(usuarios user) {
        this.Id = UUID.randomUUID();
        this.nombre = user.nombre;
        this.apellidos = user.apellidos;
        this.sexo = user.sexo;
        this.fechaNacimiento = user.fechaNacimiento;
        this.nacionalidad = user.nacionalidad;
        this.paisResidencia = user.paisResidencia;
        this.paisOrigen = user.paisOrigen;
    }

    public usuarios(String nombre, String apellidos, String sexo, Date fechaNacimiento, String nacionalidad, String paisResidencia, String paisOrigen) {
        this.Id = UUID.randomUUID();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.nacionalidad = nacionalidad;
        this.paisResidencia = paisResidencia;
        this.paisOrigen = paisOrigen;
    }

    public usuarios(UUID Id, String nombre, String apellidos, String sexo, Date fechaNacimiento, String nacionalidad, String paisResidencia, String paisOrigen) {
        this.Id = Id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.nacionalidad = nacionalidad;
        this.paisResidencia = paisResidencia;
        this.paisOrigen = paisOrigen;
    }

    public UUID getId() {
        return Id;
    }

    public void setId(UUID Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

}
