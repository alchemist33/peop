/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.daos;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alquimista
 */
@Entity
@Table(name = "paises")
public class paises {

    @Id
    @Column(name = "IdPais")
    private UUID IdPais;
    
    @Column(name = "nombrePais")
    private String nombrePais;
    
    @Column(name = "nacionalidad")
    private String nacionalidad;

    public paises() {
    }

    public paises(UUID IdPais, String nombrePais) {
        this.IdPais = IdPais;
        this.nombrePais = nombrePais;
    }

    public UUID getIdPais() {
        return IdPais;
    }

    public void setIdPais(UUID IdPais) {
        this.IdPais = IdPais;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

}
