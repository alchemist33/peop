/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.daos;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alquimista
 */
@Entity
@Table(name = "OpinionesUsuarios")
public class opinion {

    @Id
    @Column(name = "IdOpinion")
    private UUID IdOpinion;

    @Column(name = "IdTema")
    private String tema;

    @Column(name = "ValoracionTema")
    private String valoracion;

    @Column(name = "opinionGeneral")
    private String opinionGeneral;

    public opinion() {
        this.IdOpinion = UUID.randomUUID();
    }

    public opinion(UUID IdOpinion, String tema, String valoracion, String opinionGeneral) {
        this.IdOpinion = IdOpinion;
        this.tema = tema;
        this.valoracion = valoracion;
        this.opinionGeneral = opinionGeneral;
    }

    public UUID getIdOpinion() {
        return IdOpinion;
    }

    public void setIdOpinion(UUID IdOpinion) {
        this.IdOpinion = IdOpinion;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public String getOpinionGeneral() {
        return opinionGeneral;
    }

    public void setOpinionGeneral(String opinionGeneral) {
        this.opinionGeneral = opinionGeneral;
    }

}
