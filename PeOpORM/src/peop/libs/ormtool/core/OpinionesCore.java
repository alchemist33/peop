/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.core;

import java.util.List;
import java.util.UUID;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import static peop.libs.ormtool.core.UsuariosCore.factory;
import static peop.libs.ormtool.core.UsuariosCore.session;
import peop.libs.ormtool.daos.opinion;
import peop.libs.ormtool.daos.usuarios;

/**
 *
 * @author alquimista
 */
public class OpinionesCore {

    SessionFactory factory = new Configuration().configure("peop/libs/ormtool/core/hibernate.cfg.xml").addAnnotatedClass(opinion.class).buildSessionFactory();
    Session session = factory.openSession();

    public void nuevaOpinion(opinion op) {

        opinion o = new opinion();
        o.setIdOpinion(UUID.randomUUID());
        o.setValoracion(op.getValoracion());
        o.setOpinionGeneral(op.getOpinionGeneral());
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();

        System.out.println("registro insertado con exito");

        session.getTransaction().commit();
        session.close();
        factory.close();

    }

    public List<opinion> getAllOpinions() {

        try {
            session.beginTransaction();
            String hql = "FROM OpinionesUsuarios";
            Query query = session.createQuery(hql);
            List<opinion> result = (List<opinion>) query.list();
            session.getTransaction().commit();
            session.close();
            factory.close();

            return result;

        } catch (Exception e) {
            //Log
        } finally {
            session.close();
            factory.close();
        }
        return null;

    }

}
