/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.core;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import peop.libs.ormtool.daos.TemasOpinion;
import peop.libs.ormtool.daos.usuarios;

/**
 *
 * @author alquimista
 */
public class TemasOpinion_Core {

     SessionFactory factory = new Configuration().configure("peop/libs/ormtool/core/hibernate.cfg.xml").addAnnotatedClass(TemasOpinion.class).buildSessionFactory();
     Session session= factory.openSession();
    public List<TemasOpinion> getTemasOpinion() {

        List<TemasOpinion> lst = new ArrayList<>();
        session.beginTransaction();
        String hql = "FROM TemasOpinion";
        Query query = session.createQuery(hql);
        lst = query.list();
        session.getTransaction().commit();
        session.clear();
        session.close();
        factory.close();

        return lst;

    }

}
