/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import org.hibernate.Query;
import peop.libs.ormtool.daos.usuarios;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import peop.libs.ormtool.daos.paises;

/**
 *
 * @author alquimista
 */
public class UsuariosCore {

    static final SessionFactory factory = new Configuration().configure("peop/libs/ormtool/core/hibernate.cfg.xml").addAnnotatedClass(usuarios.class).buildSessionFactory();
    static final Session session = factory.openSession();

    public UsuariosCore() {
    }

    public void insertUsuario(usuarios u) {

        try {
            usuarios user = new usuarios(u);
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();

            System.out.println("Nuevo Usuario registrado con exito");

            session.getTransaction().commit();
            session.close();
            factory.close();

        } catch (Exception e) {
            //LOG
        } finally {
            session.close();
            factory.close();
        }

    }

    public usuarios DatosUsuario(UUID IdUsuario) {

        try {

            session.beginTransaction();
            String hql = "FROM usuarios U WHERE U.Id='" + IdUsuario + "'";
            Query query = session.createQuery(hql);
            usuarios result = (usuarios) query.getSingleResult();

            session.getTransaction().commit();
            session.close();
            factory.close();

            return result;
        } catch (Exception e) {
            //Log
        } finally {

            session.close();
            factory.close();

        }
        return null;
    }

    public UUID getIdUsuario(String nombreUsuario) {

        try {
            session.beginTransaction();
            String hql = "FROM usuarios U WHERE U.nombre='" + nombreUsuario + "'";
            Query query = session.createQuery(hql);
            usuarios result = (usuarios) query.getSingleResult();
            session.getTransaction().commit();
            session.close();
            factory.close();

            return result.getId();

        } catch (Exception e) {
            //Log
        } finally {
            session.close();
            factory.close();
        }
        return null;

    }

    public List<usuarios> getAllUsers(){
        List<usuarios> lstUsers;
        lstUsers = new ArrayList<>();
        
        try {
             session.beginTransaction();
            String hql = "FROM usuarios";
            Query query = session.createQuery(hql);
             lstUsers = (List<usuarios>)query.list();
            session.getTransaction().commit();
            session.close();
            factory.close();
            return lstUsers;
            
        } catch (Exception e) {
           //Log
        } finally {
            session.close();
            factory.close();
        }

        return null;
        
    }
    private static final Logger LOG = Logger.getLogger(UsuariosCore.class.getName());
}
