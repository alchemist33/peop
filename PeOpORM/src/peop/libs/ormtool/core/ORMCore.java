/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.ormtool.core;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import peop.libs.ormtool.daos.TemasOpinion;
import peop.libs.ormtool.daos.opinion;
import peop.libs.ormtool.daos.paises;
import peop.libs.ormtool.daos.usuarios;

/**
 *
 * @author alquimista
 */
public class ORMCore {

    UsuariosCore uc = new UsuariosCore();
    TemasOpinion_Core toc = new TemasOpinion_Core();
    OpinionesCore oc = new OpinionesCore();
    

    // <editor-fold defaultstate="collapsed" desc="Usuarios">
    public void nuevoUsuario(usuarios u) {

        try {
            uc.insertUsuario(u);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

    }

    public usuarios datosUsuario(UUID IdUsuario) {
        return uc.DatosUsuario(IdUsuario);
    }

    public UUID getIdUsuario(String nombreUsuario) {
        return uc.getIdUsuario(nombreUsuario);
    }

    public List<usuarios> getAllUsers(){
       return uc.getAllUsers();
    }
    

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Opiniones">
    public List<TemasOpinion> verTemas() {

        List<TemasOpinion> lst = new ArrayList<>();
        try {

            lst = toc.getTemasOpinion();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return lst;
    }

    public void nuevaOpinion(opinion op) {

        try {
            oc.nuevaOpinion(op);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public List<opinion> getAllOpinions() {
        return oc.getAllOpinions();

    }
// </editor-fold>
}
