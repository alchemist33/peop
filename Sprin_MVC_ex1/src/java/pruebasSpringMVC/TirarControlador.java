/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebasSpringMVC;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author alquimista
 */
@Controller
@RequestMapping("/secundario")
public class TirarControlador {

    @RequestMapping("/muestraFormulario")
    public String muestraFormulario() { // proporciona el formualrio

        return "HolaAlumnosFormulario";
    }

    @RequestMapping("/procesarFormulario2")
    //public String procesarFormulario2(HttpServletRequest request, Model modelo)   {
    public String procesarFormulario2(@RequestParam("nombreAlumno") String nombre, Model modelo) {

        //String nombre = request.getParameter("nombreAlumno");
        nombre += " es el peor Alumno";

        String mensajeFinal = "¿Quien es el peor alumno? " + nombre;

        //agregando info al modelo
        modelo.addAttribute("mensajeClaro", mensajeFinal);

        return "HolaAlumnosSpring";
    }

}
