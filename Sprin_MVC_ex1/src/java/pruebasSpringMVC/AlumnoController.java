/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebasSpringMVC;

import javax.validation.Valid;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author alquimista
 */
@Controller
@RequestMapping("/alumno")
public class AlumnoController {

    @InitBinder
    public void miBinder(WebDataBinder binder) {

        StringTrimmerEditor recortaEspacios = new StringTrimmerEditor(true);
        binder.registerCustomEditor(String.class, recortaEspacios);

    }

    @RequestMapping("/muestraFormulario")
    public String muestraFormulario(Model modelo) {

        Alumno alu = new Alumno();

        modelo.addAttribute("alumno", alu);

        return "alumnoRegistroFormulario";
    }

    @RequestMapping("/procesarFormulario")
    public String procesarFormulario(@Valid @ModelAttribute("alumno") Alumno alumno,
            BindingResult resultadoValidacion) {

        if (resultadoValidacion.hasErrors()) {
            return "alumnoRegistroFormulario";
        } else {
            return "confirmacionRegistroAlumno";
        }
    }

}
