/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebasSpringMVC.validacionespersonalizadas;

import java.lang.annotation.Annotation;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author alquimista
 */
public class CPostalMadridValidacion implements ConstraintValidator<CPostalMadrid, String> {

    private String prefijoCodigoMadrid;

    @Override
    public void initialize(CPostalMadrid elCodigo) {
        prefijoCodigoMadrid = elCodigo.value();
        //ConstraintValidator.super.initialize(elCodigo); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        boolean valCodigo;
        if (t != null) {
            valCodigo = t.startsWith(prefijoCodigoMadrid);
        } else {
            valCodigo = true;
            return valCodigo;
        }

        return valCodigo;
    }

}
