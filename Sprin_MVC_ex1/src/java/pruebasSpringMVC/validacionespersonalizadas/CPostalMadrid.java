/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebasSpringMVC.validacionespersonalizadas;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author alquimista
 */
@Documented
@Constraint(validatedBy = CPostalMadridValidacion.class) //futura clase que contendra la logica de la validacion
@Target({ElementType.METHOD, ElementType.FIELD}) // destino de nuestra validacion a meteodos y campos 
@Retention(RetentionPolicy.RUNTIME) // check de la anotacion en tiempo de ejeccion
public @interface CPostalMadrid {

    public String value() default "28";

    public String message() default "El codigo postal debe empezar por 28";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
