<%-- 
    Document   : alumnoRegistroFormulario
    Created on : 13 dic. 2020, 11:34:17
    Author     : alquimista
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulario de Registro</title>
    </head>
    <body>
        <h1>Formulario de Registro</h1>
        <form:form action="procesarFormulario" modelAttribute="alumno">
            Nombre: <form:input path="nombre"/> <form:errors path="nombre" style="color:red"/><br/>
            Apellido: <form:input path="apellido"/><br/>
            Edad <form:input path="edad"/> <form:errors path="edad" style="color:red"/><br/><br/>
            Email <form:input path="email"/> <form:errors path="email" style="color:red"/><br/><br/>
            Codigo Postal <form:input path="codigoPostal"/> <form:errors path="codigoPostal" style="color:red"/><br/><br/>
            Asignaturas Optativas:<br/>
            <form:select path="optativa" multiple="true">
                <form:option value="Diseño" label="Diseño"></form:option>
                <form:option value="Karate" label="Karate"></form:option>
                <form:option value="Danza" label="Danza"></form:option>
            </form:select> <br/>


            Barcelona<form:radiobutton path="ciudadEstudios" value="Barcelona"/>
            Madrid<form:radiobutton path="ciudadEstudios" value="Madrid"/>
            Valencia<form:radiobutton path="ciudadEstudios" value="Valencia"/>
            Bilbao<form:radiobutton path="ciudadEstudios" value="Bilbao"/>
            <br/>

            Ingles<form:checkbox path="idiomasAlumno" value="Ingles"/>
            Frances<form:checkbox path="idiomasAlumno" value="Frances"/>
            Aleman<form:checkbox path="idiomasAlumno" value="Aleman"/>
            Chino<form:checkbox path="idiomasAlumno" value="Chino"/>

            <br/>
            <input type="submit" value="Enviar">

        </form:form>
    </body>
</html>
