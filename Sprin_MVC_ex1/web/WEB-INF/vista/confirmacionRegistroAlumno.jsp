<%-- 
    Document   : confirmacionRegistroAlmuno
    Created on : 13 dic. 2020, 12:03:46
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro completado</title>
    </head>
    <body>
        <h1>Registro completado</h1>
        
        <p>
            El almuno con nombre, <strong>${alumno.nombre}</strong>
            apellido, <strong>${alumno.apellido}</strong>,edad <strong>${alumno.edad}</strong>,
            email <strong>${alumno.email}</strong> y codigo postal <strong>${alumno.codigoPostal}</strong>se ha registrado correctamente.<br/>
            La asignatura escogida es: <strong>${alumno.optativa}</strong><br/>
            La ciudad donde iniciará el alumno los estudios es : <strong>${alumno.ciudadEstudios}</strong><br/>
            Los idiomas escogidos por el alumno son: <strong>${alumno.idiomasAlumno}</strong>
            
        </p>
    </body>
</html>
