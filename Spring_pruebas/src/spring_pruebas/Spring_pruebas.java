/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring_pruebas;

import es.pruebas.IoC.DirectorEmpleado;
import es.pruebas.IoC.SecretarioEmpleado;

import es.pruebas.IoC.empleados;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author alquimista
 */
public class Spring_pruebas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Creacion de objetos  tipo empleado
        //empleados empleado1 = new DirectorEmpleado();
        //Uso de objetos
        ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("file:src/es/pruebas/IoC/applicationContext3.xml");

//        empleados marco = contexto.getBean("miEmpleado",empleados.class);
//        
//          System.out.println(marco.getTareas());
//          System.out.println(marco.getInforme());
//          SecretarioEmpleado marco = contexto.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
//          SecretarioEmpleado alex = contexto.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
//          DirectorEmpleado marco= contexto.getBean("miEmpleado",DirectorEmpleado.class);
//          System.out.println(marco.getTareas());
//          System.out.println(marco.getInforme());
//          System.out.println(marco.getEmail());
//          System.out.println(marco.getNombreEmpresa());
//          System.out.println(marco.getEmail());
//          System.out.println(marco.getNombreEmpresa());
//      
//        SecretarioEmpleado marco = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
//        SecretarioEmpleado alex = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
//        SecretarioEmpleado ana = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
//        SecretarioEmpleado lucia = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
//        System.out.println(marco);
//        System.out.println(alex);
//        System.out.println(ana);
//        System.out.println(lucia);
//        
//        if (marco == alex) {
//            System.out.println("El mismo Objeto");
//        } else {
//
//            System.out.println("Objetos distintos");
//        }

    empleados marco = contexto.getBean("miEmpleado",empleados.class);
    System.out.println(marco.getInforme());
    contexto.close ();

}

}
