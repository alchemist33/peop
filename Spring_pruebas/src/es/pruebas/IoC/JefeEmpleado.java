/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.pruebas.IoC;

/**
 *
 * @author alquimista
 */
public class JefeEmpleado implements empleados{
private final informe informeNuevo;
    
    
    public JefeEmpleado(informe informeNuevo) {
        this.informeNuevo = informeNuevo;
    }
     
    
    public String getTareas() {
         return "Gestion a mis empleados";
    }

    @Override
    public String getInforme() {
         return "Informe del Jefe" + informeNuevo.getInforme(); 
    }
    
}
