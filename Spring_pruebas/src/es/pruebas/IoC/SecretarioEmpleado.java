/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.pruebas.IoC;

/**
 *
 * @author alquimista
 */
public class SecretarioEmpleado implements empleados {

    private  informe informeNuevo;
    private  String email;
    private  String nombreEmpresa;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
    

    public void setInformeNuevo(informe informeNuevo) {
        this.informeNuevo = informeNuevo;
    }
    
    public SecretarioEmpleado() {
    }

    @Override
    public String getTareas() {
        return "Agenda de Jefes";
    }

    @Override
    public String getInforme() {
        return "Informe del secreterio" + informeNuevo.getInforme();
    }

}
