/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.pruebas.IoC;

/**
 *
 * @author alquimista
 */
public class DirectorEmpleado implements empleados {

    private final informe informeNuevo;
    private  String email;
    private  String nombreEmpresa;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
    
    
    public DirectorEmpleado(informe informenuevo) {
        this.informeNuevo = informenuevo;
    }
 
//metodo Init. Ejecutar tareas antes de que el bean este disponible
    
    public void metodoInicial(){
        System.out.println("Dentro del metodo Init.");
    }
    
    //metodo destroy. Ejecutar tareas desùes de que el bean haya sido utilizado
    public void metodoFinal(){
        System.out.println("Dentro del metodo Destroy.");
    }
    
    @Override
    public String getTareas() {
return "GEstiono la empresa"        ;
    }

    @Override
    public String getInforme() {
        return "Informe del Directoror" + informeNuevo.getInforme();    }
    
}
