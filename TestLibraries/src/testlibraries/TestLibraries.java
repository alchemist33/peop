/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testlibraries;

/**
 *
 * @author alquimista
 */
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import peop.libs.log.dao.log;
import peop.libs.poolconnection.PoolConnectionCore;
import peop.libs.poolconnection.DbInfo;

import peop.libs.poolconnection.PoolConnectionCore.tipoConex;
import peop.libs.log.core.LogCore;
import peop.libs.log.core.LogCore.logStatus;
import peop.libs.ormtool.core.ORMCore;

import peop.libs.ormtool.daos.opinion;

import peop.libs.poolconnection.dao.CategoriasTemas;
import peop.libs.poolconnection.dao.SubCategoriasTemas;
import peop.libs.poolconnection.dao.TemasOpinion;
import peop.libs.poolconnection.dao.options_OpinionGeneral;
import peop.libs.poolconnection.dao.paises;
import peop.libs.poolconnection.dao.paises2;
import peop.libs.poolconnection.dao.usuarios;
import peop.libs.poolconnection.datakit.ClientAccessData;

public class TestLibraries {

    public static final PoolConnectionCore pcc = new PoolConnectionCore();
    //public static final ORMCore ormc = new ORMCore();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

//       LogCore lc = new LogCore();
//        
//            log l = new log();
//            l.guid = UUID.randomUUID();0
//            l.Titulo = "Prueba de librerias";
//            l.Descripcion = "Probando desde el Main de prueba de librerias";
//            l.Status = logStatus.INFO;
        //lc.insertLog(l);
        //Connection conex = pcc.getConex(tipoConex.SQLite);
//        DbInfo db = new DbInfo();
//        db.getInfoDb();
//        
        //getIdsUsuarios();
        //getOpiniones();
        //getCategorias();
        //getSubCategorias();
        //getTemas();
        // getOptionsOG();
        //getPaies();
        //nuevoUsuario();
        //checkCredentials();
        //editaUser();
        getHost();
    }

    public static void getIdsUsuarios() {

//        UUID idUsuario = ormc.getIdUsuario("Marco2");
//
//        System.out.println("El id de usuario es: " + idUsuario);
    }

    private static void getOpiniones() {

        List<opinion> ops = new ArrayList<>();
        //ops = ormc.getAllOpinions();

//        for (opinion op : ops) {
//
//            System.out.println("La valoracion es : " + op.getValoracion());
//
//        }
    }

    private static void getCategorias() {
        ClientAccessData cad = new ClientAccessData();

        for (CategoriasTemas ct : cad.getCategoriasTemas()) {

            System.out.println(ct.getNombreCategoria());

        }
    }
    
    

    private static void getSubCategorias() {
        ClientAccessData cad = new ClientAccessData();

        for (SubCategoriasTemas ct : cad.getSubCategoriasTemas()) {

            System.out.println(ct.getNombreSubCategoria());

        }
    }

    private static void getTemas() {
        ClientAccessData cad = new ClientAccessData();

        for (TemasOpinion to : cad.getTemasOpinion()) {

            System.out.println(to.getNombreTema());

        }
    }

    private static void getOptionsOG() {
        ClientAccessData cad = new ClientAccessData();

        for (options_OpinionGeneral op : cad.getOptions_OpinionGeneral()) {

            System.out.println(op.getOpcion());

        }
    }

    private static void getPaies() {
        ClientAccessData cad = new ClientAccessData();

        for (paises2 p : cad.getPaises()) {

            System.out.println(p.getNombrePais());
            System.out.println(p.getIdPais());

        }
    }

    private static void nuevoUsuario() {
        ClientAccessData cad = new ClientAccessData();

        usuarios user = new usuarios();
        user.setId(UUID.randomUUID().toString());
        user.setNombre("Pony");
        user.setApellidos("Pisador");
        user.setFechaNacimiento(new Date(04,11,2021));
        user.setNacionalidad("Española");
        user.setPaisOrigen("España");
        user.setPaisResidencia("España");
        user.setSexo("Hombre");
        user.setUsuarioPeOp("user123");
        user.setUserPass("12345");
        cad.nuevoUsuario(user);
        System.out.println("Usuario agregado");

    }
    
    private static void checkCredentials() {
        
          ClientAccessData cad = new ClientAccessData();
          System.out.println("Acceso concedido?:"+ cad.checkCredentials("Marco", "987654321"));
        
        
    }
    
    private static void editaUser(){
         ClientAccessData cad = new ClientAccessData();
         
         usuarios user = new usuarios();
        user.setId("65927E7E-8F60-4F1B-8313-0EF3A275BE01");
        user.setNombre("Pony");
        user.setApellidos("Pisador");
        user.setFechaNacimiento(new Date(04,11,2021));
        user.setNacionalidad("Española");
        user.setPaisOrigen("España");
        user.setPaisResidencia("España");
        user.setSexo("Hombre");
        user.setUsuarioPeOp("user123");
        user.setUserPass("12345");
         
         
          System.out.println("Usuario actualizado ?:" + cad.editaDatosUsuarios(user));
         
         
        
    }

    private static void getHost() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String hostname = addr.getHostName();
            System.out.println(hostname);
        } catch (UnknownHostException ex) {
            Logger.getLogger(TestLibraries.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
