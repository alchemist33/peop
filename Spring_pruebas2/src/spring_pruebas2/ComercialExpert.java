/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring_pruebas2;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 *
 * @author alquimista
 */
//@Component("ComercialExp")


@Component
@Scope("singleton")
public class ComercialExpert implements Empleados, InitializingBean, DisposableBean {

    @Autowired
    @Qualifier("informeFinancieroTrim4")
    private CreacionInformeFinanciero nuevoInforme;

// ejecucion despues de la creacion del Bena
 @Override
    public void afterPropertiesSet() throws Exception {
        //System.out.println("Ejecutado despues de la creacion del bean");
    }

// ejecucion despues del cierre del contexto    


    @Override
    public void destroy() throws Exception {
        //System.out.println("Ejecutado antes de la desctruccin");
    }

    public ComercialExpert() {
    }

//@Autowired
//    public void setNuevoInforme(CreacionInformeFinanciero nuevoInforme) {
//        this.nuevoInforme = nuevoInforme;
//    }
//    @Autowired
//    public ComercialExpert(CreacionInformeFinanciero nuevoInforme) {
//        this.nuevoInforme = nuevoInforme;
//    }
    @Override
    public String getTareas() {
        return "Tarea de comercial Exp";

    }

    @Override
    public String getInforme() {
        //return "Informe Comercial Exp";
        return nuevoInforme.getInformeFinanciero();
    }

   

}
