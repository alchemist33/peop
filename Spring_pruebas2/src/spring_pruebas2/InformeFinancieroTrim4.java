/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring_pruebas2;

import org.springframework.stereotype.Component;

/**
 *
 * @author alquimista
 */
@Component
public class InformeFinancieroTrim4 implements CreacionInformeFinanciero {

    @Override
    public String getInformeFinanciero() {
        return "Presenteacion de informe de cierre año";
    }

}
