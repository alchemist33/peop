/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring_pruebas2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author alquimista
 */
@Configuration
@ComponentScan("spring_pruebas2")
@PropertySource("file:src/spring_pruebas2/datosEmpresas.properties")
public class EmpleadosConfig {
    
    //Definir el bean para Informe Financiero departamento COMPRAS
    
    @Bean
    public CreacionInformeFinanciero informeFinancieroDtoCompras(){ //Id del ben inyectado
        
        return new InformeFinancieroDptoCompras();
    }
    
    //definir el bean para Director Financiero e inyectar dependencias
    
    @Bean
    public Empleados directorFinanciero(){
        return new DirectorFinanciero(informeFinancieroDtoCompras());
    }
    
}
