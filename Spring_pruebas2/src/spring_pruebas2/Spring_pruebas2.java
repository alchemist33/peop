/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring_pruebas2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author alquimista
 */
public class Spring_pruebas2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("file:src/spring_pruebas2/applicationContext.xml");
        AnnotationConfigApplicationContext contexto = new AnnotationConfigApplicationContext(EmpleadosConfig.class);
        //Empleados marco = contexto.getBean("comercialExpert", Empleados.class);
//        Empleados marco = contexto.getBean("comercialExpert", Empleados.class);
//        Empleados alex = contexto.getBean("comercialExpert", Empleados.class);

//        Empleados marco = contexto.getBean("directorFinanciero", Empleados.class);
//        System.out.println(marco.getInforme());
//        System.out.println(marco.getTareas());
//        if (marco == alex) {
//            System.out.println("Apuntan a la misma direccion de memoria");
//            System.out.println(marco + "\n" + alex);
//        } else {
//            System.out.println("NO apuntan a la misma direccion de memoria");
//            System.out.println(marco + "\n" + alex);
//        }
        DirectorFinanciero empleado = contexto.getBean("directorFinanciero", DirectorFinanciero.class);
        
        System.out.println("Email del director: " + empleado.getEmail());
         System.out.println("Nombre de la empresa: " + empleado.getNombreEmpresa());

        contexto.close();

// Con clase de configuracion
    }

}
