/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.datakit;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
public class UpdateData {

    public boolean editaDatosUsuarios(Connection con, usuarios user) {

        boolean updateOk = false;
        try {

            String query = "UPDATE usuarios set nombre = '" + user.getNombre() + "',"
                    + "apellidos = '" + user.getApellidos() + "', "
                    + "fechaNacimiento = '" + user.getFechaNacimiento() + "', "
                    + "paisOrigen = '" + user.getPaisOrigen() + "', "
                    + "paisResidencia = '" + user.getPaisResidencia() + "', "
                    + "Sexo = '" + user.getSexo() + "' "
                    + " WHERE Id = '" + user.getId() + "' ";

            Statement stm = con.createStatement();

            int updates = stm.executeUpdate(query);
            if (updates > 0) {
                updateOk = true;
            }
            con.close();
        } catch (SQLException e) {
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(UpdateData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return updateOk;

    }

}
