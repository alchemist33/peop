/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.datakit;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alquimista
 */
public class DeleteData {

    public boolean borrarUsuario(Connection con, String IdUsuario) {
        boolean usuarioBorrado = false;
        try {

            String query = "DELETE FROM Usuarios WHERE Id = '" + IdUsuario + "'";
            Statement stm = con.createStatement();
            usuarioBorrado = stm.execute(query);

        } catch (SQLException e) {
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DeleteData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return usuarioBorrado;
    }
    
    public boolean borrarPais(Connection con, String IdPais) {
        boolean paisBorrado = false;
        try {

            String query = "DELETE FROM paises WHERE IdPais = '" + IdPais + "'";
            Statement stm = con.createStatement();
            paisBorrado = stm.execute(query);

        } catch (SQLException e) {
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DeleteData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return paisBorrado;
    }
    //OG = OpinionGeneral
    public boolean borrarOpcion_OG(Connection con, String IdOpcion) {
        boolean opcionBorrada = false;
        try {
            String query = "DELETE FROM options_OpinionGeneral WHERE IdOption_OpinionGeneral = '" + IdOpcion + "'";
            Statement stm = con.createStatement();
            opcionBorrada = stm.execute(query);

        } catch (SQLException e) {
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DeleteData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return opcionBorrada;
    }

}
