/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.datakit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author alquimista
 */
public class CheckData {

    public boolean checkCredentials(Connection con, String userName, String userPass) {

        boolean accesoConcedido = false;
        try {
            String query = "SELECT Id FROM usuarios WHERE nombre = '" + userName + "' ";

            Statement stm = con.createStatement();
            Statement stm2 = con.createStatement();

            ResultSet rs = stm.executeQuery(query);
            String IdUsuario = "";

            if (rs != null) {

                IdUsuario = rs.getString("Id");
            }

            String query2 = "SELECT pass FROM userPasswords WHERE IdUser = '" + IdUsuario + "' ";
            if (IdUsuario != null && !"".equals(IdUsuario)) {
                ResultSet rs2 = stm2.executeQuery(query2);
                if(rs2 != null) {
                    accesoConcedido = userPass.equals(rs2.getString("pass"));
                }
                
            }
            con.close();
        } catch (SQLException e) {
            //Log
        } finally {
            try {

                con.close();

            } catch (SQLException ex) {
                Logger.getLogger(CheckData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return accesoConcedido;
    }

}
