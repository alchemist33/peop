/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.datakit;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

import peop.libs.poolconnection.PoolConnectionCore;
import peop.libs.poolconnection.dao.*;

/**
 *
 * @author alquimista
 */
public class ClientAccessData {

    GetData gd = new GetData();
    SetData sd = new SetData();
    CheckData cd = new CheckData();
    UpdateData ud = new UpdateData();
    DeleteData dd = new DeleteData();

    public List<CategoriasTemas> getCategoriasTemas() {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getCategoriasTemas(con);
    }

    public List<SubCategoriasTemas> getSubCategoriasTemas() {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getSubCategoriasTemas(con);
    }

    public List<TemasOpinion> getTemasOpinion() {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getTemasOpinion(con);
    }

    public List<options_OpinionGeneral> getOptions_OpinionGeneral() {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getOptions_OpinionGeneral(con);
    }

    public void nuevaOpinion(opinion op) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        sd.nuevaOpinion(con, op);
    }

    public void nuevoUsuario(usuarios user) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        sd.nuevoUsuario(con, user);
        userPass up = new userPass();
        up.setIdPass(UUID.randomUUID().toString().toUpperCase());
        up.setIdUser(user.getId());
        up.setPass(user.getUserPass());
        nuevaPass(up);
    }

    public void nuevaPass(userPass up) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        sd.nuevaPass(con, up);
    }

    public List<paises2> getPaises() {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getPaises(con);
    }

    public boolean checkCredentials(String userName, String userPass) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return cd.checkCredentials(con, userName, userPass);

    }

    public String getIdusuario(String nombreUsuario) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getIdUsuario(con, nombreUsuario);
    }

    public usuarios getUsuario(String userName) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getUsuario(con, userName);
    }

    public usuarios getUsuarioById(String IdUsuario) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getUsuarioById(con, IdUsuario);
    }

    public boolean editaDatosUsuarios(usuarios user) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return ud.editaDatosUsuarios(con, user);
    }

    public boolean borrarUsuario(String IdUsuario) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return dd.borrarUsuario(con, IdUsuario);
    }

    public void nuevoPais(paises2 pais) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        sd.nuevoPais(con, pais);
    }

    public boolean borrarPais(String IdPais) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return dd.borrarPais(con, IdPais);
    }

    public boolean borrarOpcion_OG(String IdOpcion) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return dd.borrarOpcion_OG(con, IdOpcion);
    }

    public void nuevaOpcion_OG(options_OpinionGeneral opcion) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        sd.nuevaOpcion_OG(con, opcion);
    }

    public List<opinion> getOpinionesUsuario(String IdUsario) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getOpinionesUsuario(con, IdUsario);
    }
    
    public List<opinion> getOpinionesFiltro(String paisResidencia, String paisOrigen,  String fechaNacimiento, String sexo) {
        Connection con = new PoolConnectionCore().getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getOpinionesFiltro(con,paisResidencia,paisOrigen,fechaNacimiento,sexo);
    }
}
