/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.datakit;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import peop.libs.poolconnection.PoolConnectionCore;
import peop.libs.poolconnection.dao.opinion;
import peop.libs.poolconnection.dao.options_OpinionGeneral;
import peop.libs.poolconnection.dao.paises2;
import peop.libs.poolconnection.dao.userPass;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
public class SetData {

    public void nuevoUsuario(Connection con, usuarios us) {
        try {
            String query = "INSERT INTO Usuarios VALUES('" + us.getId() + "','" + us.getNombre() + "','" + us.getApellidos() + "','" + us.getFechaNacimiento()
                    + "','" + us.getNacionalidad() + "','" + us.getPaisResidencia() + "','" + us.getPaisOrigen() + "','" + us.getSexo() + "','" + us.getUsuarioPeOp() + "','" + us.getFechaAlta() + "','" + us.getEmail() + "') ";

            Statement stm = con.createStatement();
            stm.execute(query);

            con.close();

        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        } finally {
            try {
                con.close();

            } catch (SQLException ex) {
                Logger.getLogger(SetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void nuevaPass(Connection con, userPass up) {
        try {
            
            Statement stm = con.createStatement();
            String query = "INSERT INTO userPasswords VALUES('" + up.getIdPass() + "','" + up.getIdUser() + "','" + up.getPass() + "')";

            stm.execute(query);
            con.close();
        } catch (SQLException e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void nuevaOpinion(Connection con, opinion op) {
        Calendar fecha = new GregorianCalendar();

        int año = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = dateFormat.format(date);

        try {
            String NewIdOpinion = UUID.randomUUID().toString().toUpperCase();
            Statement stm = con.createStatement();
            String query = "INSERT INTO OpinionesUsuarios VALUES('" + NewIdOpinion + "','" + op.getIdUsuario() + "','" + op.getIdCategoria() + "','" + op.getIdSubCategoria() + "','" + op.getIdTema()
                    + "','" + op.getValoracion() + "','" + op.getOpinionGeneral() + "','" + op.getVom() + "','" + strDate + "') ";

            stm.executeQuery(query);
            con.close();

        } catch (SQLException ex) {
            //Log
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void nuevoPais(Connection con, paises2 pais) {

        try {

            Statement stm = con.createStatement();
            String query = "INSERT INTO paises(nombrePais,nacionalidad) VALUES('" + pais.getNombrePais() + "','" + pais.getNacionalidad() + "')";

            stm.execute(query);
            con.close();
        } catch (SQLException e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
    //OG = Opinion General

    public void nuevaOpcion_OG(Connection con, options_OpinionGeneral option) {
        try {
            Statement stm = con.createStatement();
            String query = "INSERT INTO options_OpinionGeneral(opcion,orden) VALUES('" + option.getOpcion() + "','" + option.getOrden() + "')";

            stm.execute(query);
            con.close();
        } catch (SQLException e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
