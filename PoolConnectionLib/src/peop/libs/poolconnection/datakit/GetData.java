/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.datakit;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import peop.libs.poolconnection.dao.*;
import peop.libs.poolconnection.PoolConnectionCore;

/**
 *
 * @author alquimista
 */
public class GetData {

    public List<paises2> getPaises(Connection con) {

        try {
            List<paises2> paises;
            paises = new ArrayList<>();

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM paises");

            while (rs.next()) {
                paises2 pais = new paises2();

                pais.setNacionalidad(rs.getString("nacionalidad"));
                pais.setNombrePais(rs.getString("nombrePais"));
                pais.setIdPais(rs.getInt("IdPais"));
                paises.add(pais);
            }
            con.close();
            return paises;
        } catch (SQLException ex) {
            //Log
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public List<usuarios> getAllUsers(Connection con) {

        try {
            List<usuarios> lstUsers;
            lstUsers = new ArrayList<>();

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM usuarios");

            while (rs.next()) {
                usuarios user = new usuarios();
                //InputStream input = rs.getBinaryStream("Id");
                user.setId(rs.getString("Id"));
                user.setNombre(rs.getString("nombre"));
                user.setApellidos(rs.getString("apellidos"));
                user.setFechaNacimiento(rs.getDate("fechaNacimiento"));
                user.setNacionalidad(rs.getString("nacionalidad"));
                user.setPaisResidencia(rs.getString("paisResidencia"));
                user.setPaisOrigen(rs.getString("paisOrigen"));
                user.setSexo(rs.getString("sexo"));
                user.setUsuarioPeOp(rs.getString("usuarioPeoP"));
                user.setUserPass(rs.getString("userPass"));

                lstUsers.add(user);
            }
            con.close();
            return lstUsers;
        } catch (SQLException ex) {
            //Log
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String getIdUsuario(Connection con, String nombreUsuario) {
        String IdUsuario = "";
        try {
            String query = "SELECT Id FROM usuarios WHERE nombre = '" + nombreUsuario + "' ";
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            if (rs != null) {
                IdUsuario = rs.getString("Id");
            }
            con.close();

        } catch (SQLException e) {
            //Log
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return IdUsuario;
    }

    public usuarios getUsuario(Connection con, String userName) {

        usuarios user = new usuarios();

        try {
            String query = "SELECT * FROM usuarios WHERE nombre = '" + userName + "' ";
            Statement stm = con.createStatement();

            ResultSet rs = stm.executeQuery(query);

            if (rs != null) {
                user.setId(rs.getString("Id"));
                user.setNombre(userName);
                user.setUsuarioPeOp(rs.getString("usuarioPeOp"));
                user.setApellidos(rs.getString("apellidos"));
                user.setFechaNacimiento(rs.getDate("fechaNacimiento"));
                user.setPaisOrigen(rs.getString("paisOrigen"));
                user.setPaisResidencia(rs.getString("paisResidencia"));
                user.setSexo(rs.getString("Sexo"));

            }
            con.close();
        } catch (SQLException e) {
            //log
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return user;
    }

    public usuarios getUsuarioById(Connection con, String IdUsuario) {

        usuarios user = new usuarios();

        try {
            String query = "SELECT * FROM usuarios WHERE Id = '" + IdUsuario + "' ";
            Statement stm = con.createStatement();

            ResultSet rs = stm.executeQuery(query);

            if (rs != null) {
                user.setId(IdUsuario);
                user.setNombre(rs.getString("nombre"));
                user.setUsuarioPeOp(rs.getString("usuarioPeOp"));
                user.setApellidos(rs.getString("apellidos"));
                user.setFechaNacimiento(rs.getDate("fechaNacimiento"));
                user.setPaisOrigen(rs.getString("paisOrigen"));
                user.setPaisResidencia(rs.getString("paisResidencia"));
                user.setSexo(rs.getString("Sexo"));

            }
            con.close();
        } catch (SQLException e) {
            //log
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return user;
    }

    // <editor-fold defaultstate="collapsed" desc="Categorias, Subcategorias y Tematicas">
    public List<CategoriasTemas> getCategoriasTemas(Connection con) {

        try {
            List<CategoriasTemas> catsTemas = new ArrayList<>();

            String query = "SELECT * FROM CategoriasTemas";

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);

            while (rs.next()) {
                CategoriasTemas categoria = new CategoriasTemas();

                categoria.setIdCategoria(rs.getString("IdCategoria"));
                categoria.setNombreCategoria(rs.getString("NombreCategoria"));
                catsTemas.add(categoria);
            }

            return catsTemas;
        } catch (SQLException ex) {
            Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<SubCategoriasTemas> getSubCategoriasTemas(Connection con) {

        try {
            List<SubCategoriasTemas> SubCatsTemas = new ArrayList<>();

            String query = "SELECT * FROM SubCategoriasTemas";

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);

            while (rs.next()) {
                SubCategoriasTemas SubCategoria = new SubCategoriasTemas();

                SubCategoria.setIdSubCategoria(rs.getString("IdSubCategoria"));
                SubCategoria.setIdCategoria(rs.getString("IdCategoriaTema"));
                SubCategoria.setNombreSubCategoria(rs.getString("NombreSubCategoria"));
                SubCatsTemas.add(SubCategoria);
            }
            con.close();
            return SubCatsTemas;
        } catch (SQLException ex) {
            Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<TemasOpinion> getTemasOpinion(Connection con) {

        try {
            List<TemasOpinion> TemasOp = new ArrayList<>();

            String query = "SELECT * FROM TemasOpinion";

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);

            while (rs.next()) {
                TemasOpinion tema = new TemasOpinion();

                tema.setIdTema(rs.getString("IdTema"));
                tema.setIdCategoriaTema(rs.getString("IdCategoriaTema"));
                tema.setIdSubCategoriaTema(rs.getString("IdSubCategoriaTema"));
                tema.setNombreTema(rs.getString("NombreTema"));
                TemasOp.add(tema);
            }
            con.close();
            return TemasOp;
        } catch (SQLException ex) {
            Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Opciones">
    public List<options_OpinionGeneral> getOptions_OpinionGeneral(Connection con) {
        try {
            List<options_OpinionGeneral> options_OG = new ArrayList<>();

            String query = "SELECT * FROM options_OpinionGeneral order by orden";

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);

            while (rs.next()) {
                options_OpinionGeneral option = new options_OpinionGeneral();

                option.setIdOption_OpinionGeneral(rs.getInt("IdOption_OpinionGeneral"));
                option.setOpcion(rs.getString("opcion"));
                option.setOrden(rs.getInt("orden"));

                options_OG.add(option);
            }
            con.close();
            return options_OG;
        } catch (SQLException ex) {
            Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    // </editor-fold>
    public List<opinion> getOpinionesUsuario(Connection con, String IdUsuario) {

        List<opinion> lstOps = new ArrayList<>();

        try {
            String query = "SELECT * FROM OpinionesUsuarios WHERE IdUsuario = '" + IdUsuario + "' ";
            Statement stm = con.createStatement();

            ResultSet rs = stm.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {

                    opinion op = new opinion();
                    op.setIdOpinion(rs.getString("IdOpinion"));
                    op.setIdCategoria(rs.getString("IdCategoriaTema"));
                    op.setIdSubCategoria(rs.getString("IdSubCategoriaTema"));
                    op.setIdTema(rs.getString("IdTema"));
                    op.setIdUsuario(rs.getString("IdUsuario"));
                    op.setValoracion(rs.getString("ValoracionTema"));
                    op.setOpinionGeneral(rs.getString("opinionGeneral"));
                    op.setVom(rs.getString("vom"));

                    lstOps.add(op);
                }
                

            }
            con.close();
        } catch (SQLException e) {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
            //log
        } finally {
            //log?
        }

        return lstOps;

    }
    
     public List<opinion> getOpinionesFiltro(Connection con,String paisResidencia,  String paisOrigen,  String fechaNacimiento,String sexo) {

         

        List<opinion> lstOps = new ArrayList<>();

        try {
//            String query = String.format("SELECT * FROM OpinionesUsuarios OP \n" +
//                            "LEFT JOIN usuarios US on OP.IdUsuario = US.Id\n" +
//                                "WHERE OP.IdTema = '%s' AND\n" +
//                                     "US.nacionalidad = '%s' AND\n" +
//                                      "Us.paisOrigen = '%s' AND\n" +
//                                      "Us.paisResidencia = '%s' AND\n" +
//                                      "us.fechaNacimiento = '%s'\n" +
//                                        "order by OP.dbTimeStamp desc LIMIT 12", IdTema,nacionalidad,paisOrigen,paisResidencia,fechaNacimiento); 
            
            
        String query = String.format("SELECT OP.IdTema,\n" +
"		OP.opinionGeneral,\n" +
"		OP.vom,\n" +
"		OP.IdUsuario,\n" +
"		US.apellidos,\n" +
"		US.nacionalidad, \n" +
"               US.paisOrigen, \n" +
"               US.paisResidencia, \n" +
"               US.fechaNacimiento, \n" +
"               US.sexo\n" +
"                   FROM OpinionesUsuarios OP \n" +
"                            LEFT JOIN usuarios US on OP.IdUsuario = US.Id\n" +
"                                WHERE \n" +
"                                      Us.paisResidencia = '%s' AND\n" +
"                                      Us.paisOrigen = '%s' AND\n" +                              
"                                      us.fechaNacimiento = '%s' AND\n" +
"                                      us.sexo = '%s'\n" +
"                                        order by OP.dbTimeStamp desc LIMIT 12", paisResidencia,paisOrigen,fechaNacimiento,sexo);
            
            
            Statement stm = con.createStatement();

            ResultSet rs = stm.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {

                    opinion op = new opinion();
                   
                    op.setIdTema(rs.getString("IdTema"));
                    op.setOpinionGeneral(rs.getString("opinionGeneral"));
                    op.setVom(rs.getString("vom"));
                    op.setIdUsuario(rs.getString("IdUsuario"));
               
                    
                    

                    lstOps.add(op);
                }
                

            }
            con.close();
        } catch (SQLException e) {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
            //log
        } finally {
            //log?
        }

        return lstOps;

    }
    
}
