/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection;

import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.dbcp.BasicDataSource;

/**
 *
 * @author alquimista
 */
public class PoolConnectionCore {

    private String PROPERTIES_FILE = "";
    private boolean debugEnvironment = true;
    private final Properties Props = new Properties();
  

    public PoolConnectionCore() {
        getEnvironment();
        setProperties();
    }

    private void getEnvironment() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            debugEnvironment = "devOpsUbuntu".equals(addr.getHostName());
            
            if (debugEnvironment) {
                PROPERTIES_FILE = "/home/alquimista/NetBeansProjects/KrakenRepos/PeOp/connections.properties";
            } else {
                PROPERTIES_FILE = "/peopData/connections.properties";
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(PoolConnectionCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public enum tipoConex {
        SQLite,
        Oracle,
        MySQL,
        SQLite_log,
        SQLite_test,
        Oracle_log,
        MySQL_log
    }

    public Connection getConex(tipoConex tyCon) {
        return crearConexion(tyCon);
    }

    private void setProperties() {
        try {
            Props.load(new FileReader(PROPERTIES_FILE));// Caragamos fichero de propiedades
        } catch (IOException ex) {
            Logger.getLogger(PoolConnectionCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Connection crearConexion(tipoConex typeCon) {

        BasicDataSource bdSource = new BasicDataSource();

        switch (typeCon) {

            case SQLite_test:
                bdSource.setDriverClassName("org.sqlite.JDBC");
                bdSource.setUrl("jdbc:sqlite:" + Props.getProperty("pruebasHibernate"));
                //bdSource.setUsername(P.getProperty("sqlite_user"));
                //bdSource.setPassword(P.getProperty("sqlite_pass"));
                break;

            case SQLite:
                bdSource.setDriverClassName("org.sqlite.JDBC");
                bdSource.setUrl("jdbc:sqlite:" + Props.getProperty("sqlite_db"));
                //bdSource.setUsername(P.getProperty("sqlite_user"));
                //bdSource.setPassword(P.getProperty("sqlite_pass"));
                break;

            case SQLite_log:
                bdSource.setDriverClassName("org.sqlite.JDBC");
                bdSource.setUrl("jdbc:sqlite:" + Props.getProperty("sqlite_db_log"));
                //bdSource.setUsername(P.getProperty("sqlite_user"));
                //bdSource.setPassword(P.getProperty("sqlite_pass"));
                break;

            case MySQL:
                bdSource.setDriverClassName("com.mysql.jdbc.Driver");
                //bdSource.setUrl("jdbc:mysql://localhost:3306/ejemplo/AllowMultiQueries=true");
                //bdSource.setUrl("jdbc:mysql://localhost:3306/ejemplo");
                bdSource.setUsername("ejemplo");
                bdSource.setPassword("ejemplo");

                break;

            case Oracle:
                //bdSource.setDriverClassName("com.mysql.jdbc.Driver");
                //bdSource.setUrl("jdbc:mysql://localhost:3306/ejemplo");
                bdSource.setUsername("ejemplo");
                bdSource.setPassword("ejemplo");
                break;
        }

        Connection con = null;

        try {
            con = bdSource.getConnection();
            System.out.println("Conexion del tipo " + typeCon.name() + " Creada!");
        } catch (SQLException ex) {
            Logger.getLogger(PoolConnectionCore.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Error en el intento de conexion");
        }

        return con;
    }

    private static void accesoJDBC_SQLITE(String query) {

        try {
            Class.forName("org.sqlite.JDBC");
            Connection conex = DriverManager.getConnection("jdbc:sqlite:D:/DB/SQLITE/ejemplo.db");
            Statement sentencia = conex.createStatement();
            //String sql = "SELECT * FROM departamentos";

            ResultSet result = sentencia.executeQuery(query);

            while (result.next()) {
                System.out.printf("%d, %s, %s, %n", result.getInt(1), result.getString(2), result.getString(3));
            }

            result.close();
            sentencia.close();
            conex.close();

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PoolConnectionCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
