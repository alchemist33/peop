package peop.libs.poolconnection;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import peop.libs.poolconnection.PoolConnectionCore.tipoConex;




/**
 *
 * @author marco
 */
public class DbInfo {

    PoolConnectionCore pcc = new PoolConnectionCore();
    
    
    public void getInfoDb() {

    Connection con = pcc.getConex(tipoConex.SQLite_log);

        try {
            DatabaseMetaData dbmt = con.getMetaData();

            ResultSet result = null;
            String nombre = dbmt.getDatabaseProductName();
            String driver = dbmt.getDriverName();
            String url = dbmt.getURL();
            String usuario = dbmt.getUserName();

            System.out.println("INFORMACION SOBRE LA BASE DE DATOS");
            System.out.println("==================================");
            System.out.printf("Nombre   : %s %n", nombre);
            System.out.printf("Driver   : %s %n", driver);
            System.out.printf("url      : %s %n", url);
            System.out.printf("usuario  : %s %n", usuario);

            result = dbmt.getTables("DB_peop_log", null, null, null);

            System.out.println("");
            System.out.println("INFORMACION SOBRE LAS TABLAS Y VISTAS");
            System.out.println("==================================");

            while (result.next()) {
                String catalogo = result.getString(1);
                String esquema = result.getString(2);
                String tabla = result.getString(3);
                String tipo = result.getString(4);
                System.out.printf("Tipo: %s, Catalogo: %s, Esquema: %s, Nombre: %s %n", tipo, catalogo, esquema, tabla);
            }

            result = dbmt.getColumns("null", "null", "ApplicationLog2020", null);

            System.out.println("");
            System.out.println("INFORMACION SOBRE LAS COLUMNAS DE LA TABLA DEPARTAMENTOS");
            System.out.println("==================================");

            while (result.next()) {
                String nomCOL = result.getString("COLUMN_NAME");
                String tipoCol = result.getString("TYPE_NAME");
                String tamCol = result.getString("COLUMN_SIZE");
                String nula = result.getString("IS_NULLABLE");

                System.out.printf("Columna: %s, Tipo: %s, Tamaño: %s, Es Nula: %s %n", nomCOL, tipoCol, tamCol, nula);

            }
            result = dbmt.getPrimaryKeys("null", "null", "ApplicationLog2020");
            System.out.println("");
            System.out.println("INFORMACION SOBRE LAS CLAVES PRIMARIAS DE LA TABLA DEPARTAMENTOS");
            System.out.println("==================================");

            while (result.next()) {
                String pkDep = result.getString("COLUMN_NAME"); //getString(4)

                System.out.printf("Calve Primaria: %s, %n", pkDep);
            }
            
            result = dbmt.getExportedKeys(null, "ejemplo", "departamentos");
            System.out.println("");
            System.out.println("INFORMACION SOBRE LAS CLAVES AJENAS (EXPORTED) QUE REFERENCIAN A LA TABLA DEPARTAMENTOS");
            System.out.println("==================================");

            while (result.next()) {
                String fk_name = result.getString("FKCOLUMN_NAME");
                String pk_name = result.getString("PKCOLUMN_NAME");
                String pk_tablename = result.getString("PKTABLE_NAME"); 
                String fk_tablename = result.getString("FKTABLE_NAME"); 

                System.out.printf("Table PK: %s, Calve Primaria: %s, %n",pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Calve Ajena: %s, %n", fk_tablename, fk_name);
            }
            
            result = dbmt.getImportedKeys("ejemplo", "ejemplo", "departamentos");
            System.out.println("");
            System.out.println("INFORMACION SOBRE LAS CLAVES AJENAS (IMPORTED) QUE REFERENCIAN A LA TABLA DEPARTAMENTOS");
            System.out.println("==================================");

            while (result.next()) {
                String fk_name = result.getString("FKCOLUMN_NAME");
                String pk_name = result.getString("PKCOLUMN_NAME");
                String pk_tablename = result.getString("PKTABLE_NAME"); 
                String fk_tablename = result.getString("FKTABLE_NAME"); 

                System.out.printf("Table PK: %s, Calve Primaria: %s, %n",pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Calve Ajena: %s, %n", fk_tablename, fk_name);
            }
              result = dbmt.getProcedures("ejemplo", "departamentos", null);
            System.out.println("");
            System.out.println("INFORMACION SOBRE LOS PROCEDIMIENTOS ALMACENADOS");
            System.out.println("==================================");

            while (result.next()) {
                String proc_name = result.getString("PROCEDURE_NAME");
                String proc_type = result.getString("PROCEDURE_TYPE");

                System.out.printf("Nombre procedimiento: %s - Tipo: %s %n",proc_name, proc_type);
                
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbInfo.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
}
