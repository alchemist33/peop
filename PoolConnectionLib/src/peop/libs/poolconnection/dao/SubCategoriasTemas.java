/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.dao;

/**
 *
 * @author alquimista
 */
public class SubCategoriasTemas {

    private String IdSubCategoria;
    private String IdCategoria;
    private String nombreSubCategoria;

    public SubCategoriasTemas() {
    }

    public SubCategoriasTemas(String IdSubCategoria, String IdCategoria, String nombreSubCategoria) {
        this.IdSubCategoria = IdSubCategoria;
        this.IdCategoria = IdCategoria;
        this.nombreSubCategoria = nombreSubCategoria;
    }

    public String getIdSubCategoria() {
        return IdSubCategoria;
    }

    public void setIdSubCategoria(String IdSubCategoria) {
        this.IdSubCategoria = IdSubCategoria;
    }

    public String getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(String IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    public String getNombreSubCategoria() {
        return nombreSubCategoria;
    }

    public void setNombreSubCategoria(String nombreSubCategoria) {
        this.nombreSubCategoria = nombreSubCategoria;
    }

}
