/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.dao;

/**
 *
 * @author alquimista
 */
public class TemasOpinion {

    private String IdTema;
    private String IdCategoriaTema;
    private String IdSubCategoriaTema;
    private String nombreTema;

    public TemasOpinion() {
    }

    public TemasOpinion(String IdTema, String IdCategoriaTema, String IdSubCategoriaTema, String nombreTema) {
        this.IdTema = IdTema;
        this.IdCategoriaTema = IdCategoriaTema;
        this.IdSubCategoriaTema = IdSubCategoriaTema;
        this.nombreTema = nombreTema;
    }

    public String getIdTema() {
        return IdTema;
    }

    public void setIdTema(String IdTema) {
        this.IdTema = IdTema;
    }

    public String getIdCategoriaTema() {
        return IdCategoriaTema;
    }

    public void setIdCategoriaTema(String IdCategoriaTema) {
        this.IdCategoriaTema = IdCategoriaTema;
    }

    public String getIdSubCategoriaTema() {
        return IdSubCategoriaTema;
    }

    public void setIdSubCategoriaTema(String IdSubCategoriaTema) {
        this.IdSubCategoriaTema = IdSubCategoriaTema;
    }

    public String getNombreTema() {
        return nombreTema;
    }

    public void setNombreTema(String nombreTema) {
        this.nombreTema = nombreTema;
    }

}
