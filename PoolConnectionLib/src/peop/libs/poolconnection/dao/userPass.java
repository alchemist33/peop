/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.dao;

/**
 *
 * @author alquimista
 */
public class userPass {

    private String IdPass;
    private String IdUser;
    private String pass;

    public userPass() {
    }

    public userPass(String IdPass, String IdUser, String pass) {
        this.IdPass = IdPass;
        this.IdUser = IdUser;
        this.pass = pass;
    }

    public String getIdPass() {
        return IdPass;
    }

    public void setIdPass(String IdPass) {
        this.IdPass = IdPass;
    }

    public String getIdUser() {
        return IdUser;
    }

    public void setIdUser(String IdUser) {
        this.IdUser = IdUser;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
