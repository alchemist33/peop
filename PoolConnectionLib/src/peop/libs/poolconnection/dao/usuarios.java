/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.dao;

import java.sql.Date;
import java.util.Calendar;
import java.util.UUID;

/**
 *
 * @author alquimista
 */
public class usuarios {

    private String Id;
    private String nombre;
    private String apellidos;
    private String sexo;
    private Date fechaNacimiento;
    private String nacionalidad;
    private String paisResidencia;
    private String paisOrigen;
    private String usuarioPeOp;
    private String userPass;
    private Date fechaAlta;
    private String email;

    public usuarios() {
        this.Id = UUID.randomUUID().toString().toUpperCase();
        this.fechaAlta = new Date(Calendar.getInstance().getTime().getTime());
    }

    public usuarios(usuarios user) {
        this.Id = UUID.randomUUID().toString();
        this.nombre = user.nombre;
        this.apellidos = user.apellidos;
        this.sexo = user.sexo;
        this.fechaNacimiento = user.fechaNacimiento;
        this.nacionalidad = user.nacionalidad;
        this.paisResidencia = user.paisResidencia;
        this.paisOrigen = user.paisOrigen;
        this.usuarioPeOp = user.usuarioPeOp;
        this.userPass = user.userPass;
        this.fechaAlta = new Date(Calendar.getInstance().getTime().getTime());
        this.email = user.email;
    }

    public usuarios(String nombre, String apellidos, String sexo, Date fechaNacimiento, String nacionalidad, String paisResidencia, String paisOrigen, String usuarioPeOp, String userPass, String email) {
        this.Id = UUID.randomUUID().toString();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.nacionalidad = nacionalidad;
        this.paisResidencia = paisResidencia;
        this.paisOrigen = paisOrigen;
        this.usuarioPeOp = usuarioPeOp;
        this.userPass = userPass;
        this.fechaAlta = new Date(Calendar.getInstance().getTime().getTime());
        this.email = email;
    }

    public usuarios(String Id, String nombre, String apellidos, String sexo, Date fechaNacimiento, String nacionalidad, String paisResidencia, String paisOrigen, String usuarioPeOp, String userPass, Date fechaAlta, String email) {
        this.Id = Id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.nacionalidad = nacionalidad;
        this.paisResidencia = paisResidencia;
        this.paisOrigen = paisOrigen;
        this.usuarioPeOp = usuarioPeOp;
        this.userPass = userPass;
        this.fechaAlta = fechaAlta;
        this.email = email;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String getUsuarioPeOp() {
        return usuarioPeOp;
    }

    public void setUsuarioPeOp(String usuarioPeOp) {
        this.usuarioPeOp = usuarioPeOp;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
