/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.poolconnection.dao;

import java.util.UUID;

/**
 *
 * @author alquimista
 */
public class opinion {

    private String tema;
    private String valoracion;
    private String opinionGeneral;
    private String vom;
    private String IdCategoria;
    private String IdSubCategoria;
    private String IdTema;
    private String IdUsuario;
    private String IdOpinion;
    private String dbTimeStamp;

    public opinion() {
        this.IdOpinion = UUID.randomUUID().toString().toUpperCase();
    }

    public opinion(String tema, String valoracion, String opinionGeneral, String vom, String IdCategoria, String IdSubCategoria, String IdTema, String IdUsuario) {
        this.tema = tema;
        this.valoracion = valoracion;
        this.opinionGeneral = opinionGeneral;
        this.vom = vom;
        this.IdCategoria = IdCategoria;
        this.IdSubCategoria = IdSubCategoria;
        this.IdTema = IdTema;
        this.IdUsuario = IdUsuario;

}

    public opinion(String tema, String valoracion, String opinionGeneral, String vom, String IdCategoria, String IdSubCategoria, String IdTema, String IdUsuario, String IdOpinion, String dbTimeStamp) {
        this.tema = tema;
        this.valoracion = valoracion;
        this.opinionGeneral = opinionGeneral;
        this.vom = vom;
        this.IdCategoria = IdCategoria;
        this.IdSubCategoria = IdSubCategoria;
        this.IdTema = IdTema;
        this.IdUsuario = IdUsuario;
        this.IdOpinion = IdOpinion;
        this.dbTimeStamp = dbTimeStamp;
    }

  

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }

    public String getOpinionGeneral() {
        return opinionGeneral;
    }

    public void setOpinionGeneral(String opinionGeneral) {
        this.opinionGeneral = opinionGeneral;
    }

    public String getVom() {
        return vom;
    }

    public void setVom(String vom) {
        this.vom = vom;
    }

    public String getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(String IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    public String getIdSubCategoria() {
        return IdSubCategoria;
    }

    public void setIdSubCategoria(String IdSubCategoria) {
        this.IdSubCategoria = IdSubCategoria;
    }

    public String getIdTema() {
        return IdTema;
    }

    public void setIdTema(String IdTema) {
        this.IdTema = IdTema;
    }

    public String getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(String IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getIdOpinion() {
        return IdOpinion;
    }

    public void setIdOpinion(String IdOpinion) {
        this.IdOpinion = IdOpinion;
    }

    public String getDbTimeStamp() {
        return dbTimeStamp;
    }

    public void setDbTimeStamp(String dbTimeStamp) {
        this.dbTimeStamp = dbTimeStamp;
    }

    
}
