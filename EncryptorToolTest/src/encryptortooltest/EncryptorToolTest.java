/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryptortooltest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author alquimista
 */
public class EncryptorToolTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        String textoAEncriptar = "1";
        String textoADesencriptar = "X¼âß\\pwnmÕ¯jXR³ain^VdR`TYRq×np©ßT]R³©eí¡baRVnYjYdn`¼[§Ö[pURa^XR[©]jRsmVgXR^V`[f­maRf©»iypm¡h^";
        //System.out.println(EncryptText(textoAEncriptar));
        String feed = generateFeed();

        System.out.println("Final Feed: " + feed);
        String retroFeed = generateRetroFeed(feed);
        System.out.println(retroFeed);

        //System.out.println(EncryptText(textoAEncriptar, feed));
        //System.out.println(DecryptText(textoADesencriptar, feed));

//        for (int i = 33; i <= 1000; i++) {
//
//            System.out.println("#" + i + "->" + Character.toString((char) i));
//        }
    }

    public static String EncryptText(String text, String feed) {

        String EcryptedText = "";

        for (int i = 0; i < text.length(); i++) {

            for (int e = 1; e < feed.length(); e++) {

                Character CharText = text.charAt(i);
                Character CharFeed = feed.charAt(e);
                int combi = CharText + CharFeed;
                int checkcombi = CheckexcludedASCIISymbols(combi);

                EcryptedText += Character.toString((char) checkcombi);
            }
        }

        return EcryptedText;

    }

    public static String DecryptText(String text, String feed) {

        return "";
    }

    public static String generateFeed() {

        String originalFeed = UUID.randomUUID().toString().split("-")[0];
        originalFeed = originalFeed.replace("-", "");
        System.out.println("OriginalFeed: " + originalFeed);
        List<Character> cadena = new ArrayList<>();
        for (int i = 0; i < originalFeed.length(); i++) {

            cadena.add(originalFeed.charAt(i));
        }

        List<Integer> CadNumerica = new ArrayList<>();

        for (Character letter : cadena) {

            CadNumerica.add(Character.getNumericValue(letter));
        }

        int res = 0;
        String feed = "";
        for (int i = 1; i < CadNumerica.size() - 1; i++) {

            Integer digitoAnterior = CadNumerica.get(i - 1);
            Integer digitoActual = CadNumerica.get(i);
            Integer digitoSiguiente = CadNumerica.get(i + 1);

            res = digitoAnterior * (digitoActual + digitoSiguiente);
            res = CheckexcludedASCIISymbols(res);
            feed += Character.toString((char) res);

        }

        return feed;
    }

    public static String generateRetroFeed(String feed) {

        String retroFeed = "";
        String DecryptedText = "";
        int res = 0;
        for (int i = 0; i < feed.length(); i++) {

            Character caracterAnterior = feed.charAt(i - 1);
            Character caracterActual = feed.charAt(i);
            Character caracterSiguiente = feed.charAt(i + 1);

            res = caracterAnterior / (caracterActual - caracterSiguiente);
            res = CheckexcludedASCIISymbols(res);
            retroFeed += Character.toString((char) res);

        }

        return retroFeed;
    }

    public static Integer CheckexcludedASCIISymbols(Integer num) {

        List<Integer> numsKO = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32);
        List<Integer> numsKO2 = Arrays.asList(127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160);
        List<Integer> numsKO3 = Arrays.asList(888, 889, 896, 897, 898, 899, 907, 909, 930);

        for (Integer i : numsKO) {
            if (num.equals(i)) {
                num = num + 33;
            }
        }
        for (Integer i : numsKO2) {
            if (num.equals(i)) {
                num = num + 34;
            }
        }
        for (Integer i : numsKO3) {
            if (num.equals(i)) {
                num = num + 9;
            }
        }

        return num;
    }

}
