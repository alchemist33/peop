
function nuevoUsuario() {
    //alert("llega");
    var name = document.getElementById("nombreModal").value;
    var subname = document.getElementById("apellidosModal").value;
    var fechaNacimiento = document.getElementById("fechaNacimientoModal").value;
    //var paisOrigen = document.getElementById("paisOrigenModal").value;
    //var paisResidencia = document.getElementById("paisResidenciaModal").value;
    //var sexo = document.getElementById("sexoModal").value;
    var email = document.getElementById("emailModal").value;
    var usuarioPeOp = document.getElementById("usuarioPeOpModal").value;
    var contraseña = document.getElementById("passModal").value;

    var close = document.getElementById("cerrarNuevoUsuarioAdmin");
    $.ajax({
        type: 'POST',
        url: 'nuevoUsuario_adm',
        data: {nombre: name,
            apellidos: subname,
            fNacimiento: fechaNacimiento,
            mail: email,
            usPeOp: usuarioPeOp,
            pass: contraseña
        },
        success: function (response) {
            //alert("ajaxOK-" + response);
            close.click();
            $('#tablaUsuarios').fadeOut('slow').load('usuarios_admin.jsp').fadeIn("slow");
            $('#toastNuevoUsuarioAdminOK').toast('show');
//            setInterval(
//function()
//{
//$('#tablaUsuarios').fadeOut('slow').load('usuarios_admin.jsp').fadeIn("slow");
//}, 1000);



        },
        error: function (response) {
            //alert("error-" + response);
            $('#toastErrorNuevoUsuarioAdmin').toast('show');


        }
    });

}

function borrarUsuario() {
    var Id = document.getElementById("IdUsuarioaBorrarFieldAux").innerHTML;

    var close = document.getElementById("cerrarBorrarUsuarioAdmin");
    //alert(TextId);
    $.ajax({
        type: 'POST',
        url: 'borrarUsuario_adm',
        data: {idUser: Id},
        success: function (response) {
            //alert("ajaxOK-" + response);
            close.click();
            $('#tablaUsuarios').fadeOut('slow').load('usuarios_admin.jsp').fadeIn("slow");
            $('#toastBorrarUsuarioAdminOK').toast('show');

        },
        error: function (response) {
            //alert("error-" + response);
            $('#toastErrorBorrarUsuarioAdmin').toast('show');


        }
    });

}

function setIdToDelete(id) {
    document.getElementById("IdUsuarioaBorrarField").innerHTML =
            "Vas a borrar el usuario: <br/><b>*" + id + "*</b><br/>Esta accion es irreversible.<br/> ¿Deseas continuar?";
    document.getElementById("IdUsuarioaBorrarFieldAux").innerHTML = id;
    //alert("ajaxOK-");
//    $.ajax({
//        type: 'POST',
//        url: 'setIdUsuarioaBorrar',
//        data:{IdUser:id},
//        success: function (response) {
//           
//            
//            document.getElementById("IdUsuarioaBorrarField").innerHTML=" Vas a borrar el usuario"+id+" <br/> Esta accion es irreversible.<br/> ¿Deseas continuar?";
//            
//            
//        },
//        error: function (response) {
//            alert("error-" + response);
//           
//
//
//        }
//    });

}

function verOpiniones() {

    var pR = document.getElementById("filtro_paisResidencia").value;
    var pO = document.getElementById("filtro_paisOrigen").value;
    var fN = document.getElementById("filtro_fechaN").value;
    var sX = document.getElementById("filtro_sexo").value;

    $.ajax({
        type: 'POST',
        url: 'verOpinionesFiltro',
        dataType: "json",
        data: {paisResidencia: pR,
            paisOrigen: pO,
            fechaNacimiento: fN,
            sexo: sX},
        success: function (response) {
            $('table tr:not(:first-child)').remove();

            //alert("ajaxOK-" + response);

            var jsonArray = response;
            var i = 1;
            // Obteniendo todas las claves del JSON
            for (var clave in jsonArray) {
                // Controlando que json realmente tenga esa propiedad
                if (jsonArray.hasOwnProperty(clave)) {
                    // Mostrando en pantalla la clave junto a su valor
                    //alert("La clave es " + clave+ " y el valor es " + json[clave]);
                    var JsonObj = JSON.parse(jsonArray[clave]);
                }
                //alert("JsonObj: " + JsonObj.IdTema + "VOM: " + JsonObj.vom);
                document.getElementById("tablaFiltroOpiniones").insertRow(1).innerHTML = ' <td scope="row" id="pos"></td>\n\
                                                                                           <td scope="row" id="IdTema_filtro"></td>\n\
                                                                                           <td scope="row" id="OG_filtro"></td>\n\
                                                                                           <td scope="row" id="vom_filtro"></td>\n\
                                                                                           <td scope="row" id="IdUsuario_filtro"></td>\n\
                                                                                           <td scope="row" id="apellidos_filtro"></td>\n\
                                                                                           <td scope="row" id="nacionalidad_filtro"></td>\n\
                                                                                           <td scope="row" id="paisOrigen_filtro"></td>\n\
                                                                                           <td scope="row" id="paisResidencia_filtro"></td>\n\
                                                                                           <td scope="row" id="fNacimiento_filtro"></td>\n\
                                                                                           <td scope="row" id="sexo_filtro"></td>\n\
                                                                                           <td scope="row" id="reacciones_filtro">\n\
                                                                                             <button type="button" class="btn btn-primary btn-sm"><i class="fas fa-thumbs-up mr-2"></i>Reaccion positiva</button>\n\
                                                                                             <button type="button" class="btn btn-danger btn-sm" ><i class="fas fa-thumbs-down mr-2"></i>Reaccion negativa</button>\n\
                                                                                           </td>';
                document.getElementById("pos").innerHTML = i;
                document.getElementById("IdTema_filtro").innerHTML = JsonObj.IdTema;
                document.getElementById("OG_filtro").innerHTML = JsonObj.opinionGeneral;
                document.getElementById("vom_filtro").innerHTML = JsonObj.vom;
                document.getElementById("IdUsuario_filtro").innerHTML = JsonObj.IdUsuario;
                document.getElementById("apellidos_filtro").innerHTML = JsonObj.apellidos;
                document.getElementById("nacionalidad_filtro").innerHTML = JsonObj.nacionalidad;
                document.getElementById("paisOrigen_filtro").innerHTML = JsonObj.paisOrigen;
                document.getElementById("paisResidencia_filtro").innerHTML = JsonObj.paisResidencia;
                document.getElementById("fNacimiento_filtro").innerHTML = JsonObj.fechaNacimiento;
                document.getElementById("sexo_filtro").innerHTML = JsonObj.sexo;

                i++;
            }
        },
        error: function (response) {
            alert("error-" + response);
        }
    });
}

