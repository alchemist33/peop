<%-- 
    Document   : Usuarios_admin
    Created on : 12 ene. 2021, 15:07:46
    Author     : alquimista
--%>

<%@page import="java.util.UUID"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="peop.libs.poolconnection.dao.usuarios"%>

<div class="container-fluid">
    <div class="row">
        <div class="col col-sm-12">
            <h5>Administracion de usuarios</h5>
            <hr/>
        </div>
    </div>
    <div class="row justify-content-around">
        <div class="col col-sm-6 mr-0 pr-0">
            <input class="form-control mr-xl-3 " type="search" placeholder="nombre de Usuario" aria-label="Search">
        </div>
        <div class="col col-sm-4 pl-0">
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="fas fa-search mr-2"></i>Buscar</button>
        </div>
        <div class="col col-sm-auto">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#nuevoUsuarioModal"><i class="fas fa-user-plus mr-2"></i>Nuevo usuario</button>
        </div>
    </div>


    <div id="tablaUsuarios">
        <table  class="table table-sm" style="margin-top: 15px">
            <thead >
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Id Usuario </th>
                    <th scope="col">Nombre </th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Usuario PeOp</th>
                    <th scope="col">Sexo</th>
                    <th scope="col">Fecha Nacimiento</th>
                    <th scope="col">Nacionalidad</th>
                    <th scope="col">Pais Residencia</th>
                    <th scope="col">Pais Origen</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <%
                int i =1;
                List<usuarios> users = (List<usuarios>) request.getAttribute("AllUsers");
                   for(usuarios us:users){
            %>
            <tbody>
                <tr>
                    <td scope="row"><%out.write(String.valueOf(i));%></td>

                    <td scope="row"><%out.write(us.getId());%></td> 
                    <td scope="row"><%out.write(us.getNombre());%></td>
                    <td scope="row"><%out.write(us.getApellidos()); %></td>
                    <td scope="row"><%out.write(us.getUsuarioPeOp()); %></td>
                    <td scope="row"><%out.write(us.getSexo()); %></td>
                    <td scope="row"><%out.write(us.getFechaNacimiento().toString()); %></td>
                    <td scope="row"><%out.write(us.getNacionalidad()); %></td>
                    <td scope="row"><%out.write(us.getPaisResidencia()); %></td>
                    <td scope="row"><%out.write(us.getPaisOrigen()); %></td>
                    <td scope="row">

                        <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editarUsuarioModal"><i class="fas fa-user-edit mr-2"></i>Editar</button>
                        <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#eliminarUsuarioModal" onclick="setIdToDelete('<%out.write(us.getId());%>')"><i class="fas fa-user-times mr-2"></i>Eliminar</button>

                    </td>

                </tr>
            </tbody>

            <%i++;}%>
        </table>
    </div>

    <span class="badge badge-pill badge-info text-sm-left">Usuarios: <%out.write(String.valueOf(users.size()) );%></span>

</div>


<!-- Modales -->
<div class="modal fade" id="nuevoUsuarioModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">

                <h5 class="modal-title" id="staticBackdropLabel1">Nuevo usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="container-fluid">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col col-md-6">

                                <label>Nombre</label> <input type="text" id="nombreModal" class="form-control mb-2" placeholder="nombre"/>  
                                <label>Apellidos</label> <input type="text" id="apellidosModal" class="form-control mb-2" placeholder="apellidos"/>  
                                <label>E-mail</label> <input type="email" id="emailModal" class="form-control mb-2" placeholder="email"/> 
                                <label>Sexo</label>

                                <div class="form-check form-check-inline">
                                    <input id="optionHombreModal" type="radio" class="form-check-input"  value="Hombre" name="optionsSexo"/>
                                    <label class="form-check-label" for="optionHombreModal">Hombre</label>

                                    <input id="optionMujerModal" type="radio" class="form-check-input"  value="Mujer" name="optionsSexo"/>
                                    <label class="form-check-label" for="optionMujerModal">Mujer</label>

                                    <input id="optionAlternativoModal" type="radio" class="form-check-input"  value="Alternativo" name="optionsSexo"/>
                                    <label class="form-check-label" for="optionAlternativoModal">Alternativo</label>
                                </div>

                            </div>
                            <div class="col col-md-6">
                                <label>Usuario PeOp</label> <input id="usuarioPeOpModal" type="text" class="form-control mb-2" /> 
                                <label>Establece la contraseņa</label> <input id="passModal" type="password" class="form-control mb-2"/>  
                                <label>Fecha Nacimiento</label> 
                                <input  class="form-control mb-2" type="date" id="fechaNacimientoModal" name="datetime"/>  
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="toast" id="toastErrorNuevoUsuarioAdmin" role="alert" aria-live="assertive"  aria-atomic="true">
                            <div class="toast-header">
                                <img src="..." class="rounded me-2" alt="...">
                                <strong class="me-auto">PeOp Admin - Error</strong>
                                <small>Adminsitracion Interna</small>
                                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                            </div>
                            <div class="toast-body">
                                Error creando el usuario.
                            </div>
                        </div>
                        <button type="button" id="cerrarNuevoUsuarioAdmin" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="nuevoUsuario()">Crear usuario</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="editarUsuarioModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel2" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered ">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel2">Editar usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="container-fluid">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col col-md-6">

                                <label>Nombre</label> <input type="text" class="form-control mb-2" placeholder="nombre"/>  
                                <label>Apellidos</label> <input type="text" class="form-control mb-2" placeholder="apellidos"/>  
                                <label>E-mail</label> <input type="email" class="form-control mb-2" placeholder="email"/> 
                                <label>Sexo</label>

                                <div class="form-check form-check-inline">
                                    <input id="optionHombreEdit" type="radio" class="form-check-input"  value="Hombre" name="optionsSexoEdit"/>
                                    <label class="form-check-label" for="optionHombre">Hombre</label>

                                    <input id="optionMujerEdit" type="radio" class="form-check-input"  value="Mujer" name="optionsSexoEdit"/>
                                    <label class="form-check-label" for="optionMujer">Mujer</label>

                                    <input id="optionAlternativoEdit" type="radio" class="form-check-input"  value="Alternativo" name="optionsSexoEdit"/>
                                    <label class="form-check-label" for="optionAlternativo">Alternativo</label>
                                </div>

                            </div>
                            <div class="col col-md-6" >
                                <label>Usuario PeOp</label> <input type="text" class="form-control mb-2" /> 
                                <label>Establece la contraseņa</label> <input type="password" class="form-control mb-2"/>  
                                <label>Fecha Nacimiento</label> 
                                <input  class="form-control mb-2" type="date" id="datetime1" name="datetime1"/>  
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Actualizar datos</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="eliminarUsuarioModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel3" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel3">Eliminar usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="IdUsuarioaBorrarField">
                    <p>
                        <!-- texto insertado por JavaScript -->
                    </p>
                </div>
                <div id="IdUsuarioaBorrarFieldAux" class="d-none">
                    <p>
                        <!-- texto insertado por JavaScript -->
                    </p>
                </div>

                <div class="toast d-flex align-items-center" id="toastErrorBorrarUsuarioAdmin" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-body text-danger">
                        Error borrando el usuario.
                    </div>
                    <button type="button" class="btn-close ms-auto me-2" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
            <div class="modal-footer">





                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" Id="cerrarBorrarUsuarioAdmin">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="borrarUsuario()">Borrar Usuario</button>
            </div>
        </div>
    </div>
</div>


