<%@page import="peop.infraestructure.models.paisesModel"%>
<%@page import="java.util.List"%>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h6>Administraci�n Paises</h6>
            <hr/>
        </div>
        
    </div>

    <div class="row justify-content-around">
        <div class="col col-sm-6 mr-0 pr-0">
            <input class="form-control mr-xl-3 " type="search" placeholder="nombre de Pais" aria-label="Search">
        </div>
        <div class="col col-sm-4 pl-0">
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="fas fa-search"></i> Buscar</button>
        </div>
        <div class="col col-sm-auto">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#nuevoPaisModal" ><i class="fas fa-plus-circle mr-2"></i>Nuevo Pais</button>
        </div>
    </div>
    <div class ="tablaPaises">
        <div class="row">
            <table class="table table-sm" style="margin-top: 15px">
                <thead >
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Id Pais </th>
                        <th scope="col">Nombre Pais </th>
                        <th scope="col">Nacionalidad</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <%
                    int e = 1;
                    List<paisesModel> lstPaises = (List<paisesModel>) request.getAttribute("AllPaises");
                       for(paisesModel  pais: lstPaises){
                %>

                <tr>
                    <td scope="row"><%out.write(String.valueOf(e));%></td> <!-- Columna de enumeracion de  fila -->

                    <td scope="row"><%out.write(pais.getIdPais().toString());%></td> 
                    <td scope="row"><%out.write(pais.getNombrePais());%></td>
                    <td scope="row"><%out.write(pais.getNacionalidad()); %></td>
                    <td scope="row">

                        <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editarPaisModal"><i class="fas fa-edit mr-2"></i>Editar</button>
                        <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#eliminarPaisModal" onclick="setIdPaisToDelete('<%out.write(pais.getIdPais().toString());%>')"><i class="fas fa-trash-alt mr-2"></i>Eliminar</button>

                    </td>

                </tr>
                <%e++;}%>
            </table>
            <div class=" col col-sm-2">
                <span class="badge badge-pill badge-info text-sm-left">Paises: <%out.write(String.valueOf(lstPaises.size()) );%></span>
            </div>
            

        </div>
            <div class="row justify-content-end">
                <div class="col col-sm-auto">
                    <div class="toast" id="toastBorrarPaisAdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
                        <div class="toast-header">
                            <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                            <strong class="me-auto">PeOp Admin - OK</strong>
                            <small>Adminsitracion Interna</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </div>
                        <div class="toast-body">
                            Pais borrado con �xito.
                        </div>
                    </div>
                </div>
            </div>
    </div>

</div>



<!-- Modales -->

<div class="modal fade" id="nuevoPaisModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Nuevo Pais</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="container-fluid">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col col-md-6">

                                <label>Nombre Pais</label> <input id="nombrePaisModal" type="text" class="form-control mb-2" placeholder=""/>  
                                <label>Nacionalidad</label> <input id="nacionalidadModal" type="text" class="form-control mb-2" placeholder=""/>  
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="toast" id="toastErrorNuevoPaisAdmin" role="alert" aria-live="assertive"  aria-atomic="true">

                            <div class="toast-body text-danger">
                                Error creando el Pais.
                            </div>
                        </div>
                        <button type="button" id="cerrarNuevoPaisAdmin" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="nuevoPais()">Crear Pais</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="editarPaisModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered ">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Editar pais</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="container-fluid">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col col-md-6">

                                <label>Nombre Pais</label> <input type="text" class="form-control mb-2" placeholder=""/>  
                                <label>Nacionalidad</label> <input type="text" class="form-control mb-2" placeholder=""/>  
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Actualizar datos</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="eliminarPaisModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Eliminar Pais</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="IdPaisaBorrarField">
                    <p>
                        <!-- texto insertado por JavaScript -->
                    </p>
                </div>
                <div id="IdPaisaBorrarFieldAux" class="d-none">
                    <p>
                        <!-- texto insertado por JavaScript -->
                    </p>
                </div>
                <div class="toast d-flex align-items-center" id="toastErrorBorrarPaisAdmin" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-body text-danger">
                        Error borrando el Pais.
                    </div>
                    <button type="button" class="btn-close ms-auto me-2" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>

                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="cerrarBorrarPaisAdmin">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="borrarPais()">Borrar Pais</button>
            </div>
        </div>
    </div>
</div>

<script>
    function nuevoPais() {
    
     //alert("LlegaNuevo Pais-" );
    var nPais = document.getElementById("nombrePaisModal").value;
    var nacion = document.getElementById("nacionalidadModal").value;
   
     var close = document.getElementById("cerrarNuevoPaisAdmin");
    $.ajax({
        type: 'POST',
        url: 'nuevoPais_adm',
        data: {nombrePais: nPais, nacionalidad: nacion},
        success: function () {
            //alert("ajaxOK-");
            close.click();
            $('#tablaPaises').fadeOut('slow').load('Paises_admin.jsp').fadeIn("slow");
            $('#toastNuevoPaisAdminOK').toast('show');

        },
        error: function () {
            //alert("error-");
            $('#toastErrorNuevoPaisAdmin').toast('show');


        }
    });
    
}


function borrarPais(){
     var Id = document.getElementById("IdPaisaBorrarFieldAux").innerHTML;
  
     var close = document.getElementById("cerrarBorrarPaisAdmin");
     //alert(TextId);
    $.ajax({
        type: 'POST',
        url: 'borrarPais_adm',
        data:{idUser:Id},
        success: function (response) {
            //alert("ajaxOK-" + response);
            close.click();
            $('#tablaPaises').fadeOut('slow').load('Paises_admin.jsp').fadeIn("slow");
            $('#toastBorrarPaisAdminOK').toast('show');

        },
        error: function (response) {
            //alert("error-" + response);
            $('#toastErrorBorrarPaisAdmin').toast('show');


        }
    });
    
}

function setIdPaisToDelete(id){
     document.getElementById("IdPaisaBorrarField").innerHTML=
             "Vas a borrar el pais <br/><b>*"+id+"*</b><br/>Esta accion es irreversible.<br/> �Deseas continuar?";
      document.getElementById("IdPaisaBorrarFieldAux").innerHTML=id;
      }
</script>