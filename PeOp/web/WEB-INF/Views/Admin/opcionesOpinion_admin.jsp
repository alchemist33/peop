<%@page import="peop.infraestructure.models.options_OpinionGeneralModel"%>
<%@page import="java.util.List"%>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h6>Administracion Opciones de Opinion</h6>
            <hr/>
        </div>

        <div class="col col-sm-auto">
            <div class="toast" id="toastNuevaOpcion_OG_AdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
                <div class="toast-header">
                    <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                    <strong class="me-auto">PeOp Admin - OK</strong>
                    <small>Adminsitracion Interna</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    Nueva opcion creada con �xito.
                </div>
            </div>
        </div>
        


    </div>
    <div class="row justify-content-around">
        <div class="col col-sm-6 mr-0 pr-0">
            <input class="form-control mr-xl-3 " type="search" placeholder="nombre de Opcion" aria-label="Search">
        </div>
        <div class="col col-sm-4 pl-0">
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="fas fa-search mr-2"></i>Buscar</button>
        </div>
        <div class="col col-sm-auto">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#nuevaOpcionModal"><i class="fas fa-plus-circle mr-2"></i>Nueva Opcion</button>
        </div>
    </div>


    <table class="table table-sm" style="margin-top: 15px">
        <thead >
            <tr>
                <th scope="col">#</th>
                <th scope="col">Id Opcion</th>
                <th scope="col">Opcion </th>
                <th scope="col">Orden</th>
                <th scope="col">Acciones</th>

            </tr>
        </thead>
        <%
            int z = 1;
            List<options_OpinionGeneralModel> lstOptions = (List<options_OpinionGeneralModel>) request.getAttribute("options_OpinionGeneral");
               for(options_OpinionGeneralModel  option: lstOptions){
        %>

        <tr>
            <td scope="row"><%out.write(String.valueOf(z));%></td> <!-- Columna de enumeracion de  fila -->

            <td scope="row"><%=option.getIdOption_OpinionGeneral()%></td> 
            <td scope="row"><%out.write(option.getOpcion());%></td>
            <td scope="row"><%=option.getOrden()%></td>
            <td scope="row">

                <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editarOpcionModal"><i class="fas fa-user-edit"></i> Editar</button>
                <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#eliminarOpcionModal" onclick="setIdOpcion_OG_ToDelete('<%=option.getIdOption_OpinionGeneral()%>')"><i class="fas fa-user-times"></i> Eliminar</button>

            </td>

        </tr>
        <%z++;}%>

    </table>

    <div class=" col col-sm-2">
        <span class="badge badge-pill badge-info text-sm-left">Opciones: <%out.write(String.valueOf(lstOptions.size()) );%></span>
    </div>

</div>
<div class="row justify-content-end">
    <div class="col col-sm-auto">
        <div class="toast" id="toastBorrarOpcion_OG_AdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
            <div class="toast-header">
                <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                <strong class="me-auto">PeOp Admin - OK</strong>
                <small>Adminsitracion Interna</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                Opcion borrada con �xito.
            </div>
        </div>
    </div>
</div>


<!-- Modales -->

<div class="modal fade" id="nuevaOpcionModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Nueva Opcion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="container-fluid">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col col-md-6">

                                <label>Opcion</label> <input id="opcion_OGModal" type="text" class="form-control mb-2" placeholder=""/>  
                                <label>Orden</label> <input id="orden_OGModal" type="text" class="form-control mb-2" placeholder=""/>  
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="toast" id="toastErrorNuevaOpcion_OG_Admin" role="alert" aria-live="assertive"  aria-atomic="true">

                                <div class="toast-body text-danger">
                                    Error creando la opcion.
                                </div>
                            </div>
                            <button type="button" id="cerrarNuevaOpcion_OG_Admin" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary" onclick="nuevaOpcion_OG()">Crear Opcion</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="editarOpcionModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered ">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Editar opcion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <div class="container-fluid">
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col col-md-6">

                                <label>Nombre</label> <input type="text" class="form-control mb-2" placeholder="nombre"/>  
                                <label>Apellidos</label> <input type="text" class="form-control mb-2" placeholder="apellidos"/>  
                                <label>E-mail</label> <input type="email" class="form-control mb-2" placeholder="email"/> 


                            </div>
                            <div class="col col-md-6" >
                                <label>Usuario PeOp</label> <input type="text" class="form-control mb-2" /> 
                                <label>Establece la contrase�a</label> <input type="password" class="form-control mb-2"/>  

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Actualizar datos</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="eliminarOpcionModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Eliminar Opcion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="IdOpcion_OG_aBorrarField">
                    <p>
                        <!-- texto insertado por JavaScript -->
                    </p>
                </div>
                <div id="IdOpcion_OG_aBorrarFieldAux" class="d-none">
                    <p>
                        <!-- texto insertado por JavaScript -->
                    </p>

                </div>
                <div class="toast d-flex align-items-center" id="toastErrorBorrarOpcion_OG_Admin" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-body text-danger">
                        Error borrando la opcion.
                    </div>
                    <button type="button" class="btn-close ms-auto me-2" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="cerrarBorrarOpcion_OG_Admin">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="borrarOpcion_OG()">Borrar Opcion</button>
            </div>
        </div>
    </div>
</div>


<script>
function nuevaOpcion_OG() {
    
 //alert("LlegaNuevo Pais-" );
var opcion = document.getElementById("opcion_OGModal").value;
var orden = document.getElementById("orden_OGModal").value;
   
 var close = document.getElementById("cerrarNuevaOpcion_OG_Admin");
$.ajax({
    type: 'POST',
    url: 'nuevaOpcion_OG_adm',
    data: {option: opcion, order: orden},
    success: function () {
        //alert("ajaxOK-");
        close.click();
        //$('#tablaPaises').fadeOut('slow').load('Paises_admin.jsp').fadeIn("slow");
        $('#toastNuevaOpcion_OG_AdminOK').toast('show');

    },
    error: function () {
        //alert("error-");
        $('#toastErrorNuevaOpcion_OG_Admin').toast('show');


    }
});
    
}


function borrarOpcion_OG(){
   
 var Id = document.getElementById("IdOpcion_OG_aBorrarFieldAux").innerHTML;
  
 var close = document.getElementById("cerrarBorrarOpcion_OG_Admin");
  
 //alert(TextId);
$.ajax({
    type: 'POST',
    url: 'borrarOpcion_OG_adm',
    data:{idOpcion:Id},
    success: function () {
        //alert("ajaxOK-");
        close.click();
        //$('#tablaOpciones_OG').fadeOut('slow').load('opcionesOpinion_admin.jsp').fadeIn("slow");
        $('#toastBorrarOpcion_OG_AdminOK').toast('show');

    },
    error: function () {
        //alert("error-");
        $('#toastErrorBorrarOpcion_OG_Admin').toast('show');


    }
});
    
}

function setIdOpcion_OG_ToDelete(id){
 document.getElementById("IdOpcion_OG_aBorrarField").innerHTML=
         "Vas a borrar la opcion <br/><b>*"+id+"*</b><br/>Esta accion es irreversible.<br/> �Deseas continuar?";
  document.getElementById("IdOpcion_OG_aBorrarFieldAux").innerHTML=id;
  }
</script>

