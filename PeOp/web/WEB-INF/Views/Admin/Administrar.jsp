<%-- 
    Document   : Administrar
    Created on : 2 ene. 2021, 21:06:50
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administracion interna - PeOp</title>
         <%@include file='../Shared/styles.jsp' %>

    </head>
    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <%@include file='../Shared/MenuPrincipal.jsp'%>

        <div class="container-fluid mt-3">
            <div class="row">
                <div class ="col-12">
                    <h4 style="text-align: left"> Administracion Interna PeOp</h4>
                    <hr/>
                </div>
            </div>

            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-configGeneral-tab" data-bs-toggle="pill" href="#pills-configGeneral" role="tab" aria-controls="pills-configGeneral" aria-selected="true"><i class="fas fa-cog"></i> Configuracion General</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-Usuarios-tab" data-bs-toggle="pill" href="#pills-Usuarios" role="tab" aria-controls="pills-Usuarios" aria-selected="false"><i class="fas fa-user-cog"></i> Usuarios</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-configOpciones-tab" data-bs-toggle="pill" href="#pills-configOpciones" role="tab" aria-controls="pills-configOpciones" aria-selected="false"><i class="fas fa-list"></i> Configuracion Opciones</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-configGeneral" role="tabpanel" aria-labelledby="pills-configGeneral-tab"><%@include file='ConfiguracionGeneral.jsp'%></div>
                <div class="tab-pane fade" id="pills-Usuarios" role="tabpanel" aria-labelledby="pills-Usuarios-tab"><%@include file='Usuarios_admin.jsp'%></div>
                <div class="tab-pane fade" id="pills-configOpciones" role="tabpanel" aria-labelledby="pills-configOpciones-tab"><%@include file='ConfiguracionOpciones.jsp'%></div>
            </div>

        </div>

        <%@include file='../Shared/footer.jsp' %>
       
   

      