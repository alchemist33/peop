
<%@page import="peop.infraestructure.models.temasOpinionModel"%>
<%@page import="java.util.List"%>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h6>Administraci�n Temario</h6>
            <hr/>
        </div>
    </div>
    
    <div class="row justify-content-around">
        <div class="col col-sm-6 mr-0 pr-0">
            <input class="form-control mr-xl-3 " type="search" placeholder="nombre de Tema" aria-label="Search">
        </div>
        <div class="col col-sm-4 pl-0">
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="fas fa-search"></i> Buscar</button>
        </div>
        <div class="col col-sm-auto">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#nuevoPaisModal" ><i class="fas fa-plus-circle mr-2"></i>Nuevo Tema</button>
        </div>
    </div>
    <div class ="tablaTemario">
        <div class="row">
            <table class="table table-sm" style="margin-top: 15px">
                <thead >
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Id Categoria </th>
                        <th scope="col">Id SubCategoria </th>
                        <th scope="col">Id Tema</th>
                        <th scope="col">Tema</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <%
                    int g = 1;
                   
                      List<temasOpinionModel> lstTemas = (List<temasOpinionModel>) request.getAttribute("AllTemas");
                       for(temasOpinionModel  op: lstTemas){
                %>

                <tr>
                    <td scope="row"><%out.write(String.valueOf(g));%></td> <!-- Columna de enumeracion de  fila -->

                    <td scope="row"><%out.write(op.getIdCategoriaTema().toString());%></td> 
                    <td scope="row"><%out.write(op.getIdSubCategoriaTema());%></td>
                    <td scope="row"><%out.write(op.getIdTema()); %></td>
                    <td scope="row"><%out.write(op.getNombreTema()); %></td>
                    <td scope="row">

                        <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editarPaisModal"><i class="fas fa-edit mr-2"></i>Editar</button>
                        <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#eliminarPaisModal" onclick="setIdPaisToDelete('<%out.write(op.getIdTema().toString());%>')"><i class="fas fa-trash-alt mr-2"></i>Eliminar</button>

                    </td>

                </tr>
                <%g++;}%>
            </table>
            <div class=" col col-sm-2">
                <span class="badge badge-pill badge-info text-sm-left">Temas: <%out.write(String.valueOf(lstTemas.size()) );%></span>
            </div>
            

        </div>
            <div class="row justify-content-end">
                <div class="col col-sm-auto">
                    <div class="toast" id="toastBorrarTemaAdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
                        <div class="toast-header">
                            <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                            <strong class="me-auto">PeOp Admin - OK</strong>
                            <small>Adminsitracion Interna</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </div>
                        <div class="toast-body">
                            Tema borrado con �xito.
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
