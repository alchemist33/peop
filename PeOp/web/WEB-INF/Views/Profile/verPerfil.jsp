<%-- 
    Document   : verPerfil
    Created on : 27 dic. 2020, 16:02:25
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PeOp - Perfil</title>
        <%@include file='../Shared/styles.jsp' %>


        <script>
            function habilitarEdicionUsuario() {
                mostrarSpinner();

                if(document.getElementById("nombre").disabled === true){
                     document.getElementById("nombre").disabled = false; 
                } else {
                     document.getElementById("nombre").disabled = true; 
                }
                
                if(document.getElementById("apellidos").disabled === true){
                     document.getElementById("apellidos").disabled = false; 
                } else {
                     document.getElementById("apellidos").disabled = true; 
                }
                
                if(document.getElementById("paisOrigen").disabled === true){
                     document.getElementById("paisOrigen").disabled = false; 
                } else {
                     document.getElementById("paisOrigen").disabled = true; 
                }
                
                if(document.getElementById("paisResidencia").disabled === true){
                     document.getElementById("paisResidencia").disabled = false; 
                } else {
                     document.getElementById("paisResidencia").disabled = true; 
                }
                
                if(document.getElementById("fechaNacimiento").disabled === true){
                     document.getElementById("fechaNacimiento").disabled = false; 
                } else {
                     document.getElementById("fechaNacimiento").disabled = true; 
                }
                
                 if(document.getElementById("Sexo").disabled === true){
                     document.getElementById("Sexo").disabled = false; 
                } else {
                     document.getElementById("Sexo").disabled = true; 
                }
                
                 if(document.getElementById("actualizaDatos").disabled === true){
                     document.getElementById("actualizaDatos").disabled = false; 
                } else {
                     document.getElementById("actualizaDatos").disabled = true; 
                }
            }
            
           
            
            function mostrarSpinner() {
               
                var x = document.getElementById("spinner").className.valueOf();
               if(x.trim() === "visible spinner-grow spinner-grow-sm") {
                    document.getElementById("spinner").className = document.getElementById("spinner").className.replace('visible','visually-hidden',);
               } else { 
                    document.getElementById("spinner").className = document.getElementById("spinner").className.replace('visually-hidden','visible');
               }
               
               
            }
           
        </script>
       
        <script>
              editaDatosUsuario.onclick = () => {
                     alert("llega");
              const div = document.querySelector('#spinner')  ;
              if(div.className.contains("visually-hidden")){
                  div.className = "spinner-grow spinner-grow-sm";
              }
            };
        </script>
    </head>
    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">

        <%@include file='../Shared/MenuPrincipal.jsp' %>

        <div class="container-fluid mt-3">
            <div class ="row">
                <div class="col col-xl">
                    <h4 style="text-align: left">Perfil del Usuario</h4>
                </div>
            </div>
            <hr/>

            <div class ="row">
                <div class="col col-xl">

                    <h6>Datos de plataforma</h6>
                    <hr/>
                </div>
            </div>
            <form:form method="POST" cssClass="form-horizontal" action="actualizarPerfil" modelAttribute="usuario">
                <div class ="row">
                    <div class="col col-sm-6">
                        Id Usuario: <form:input cssClass="form-control form-control-sm" path="" type="text" value="${Usuario.getId()}" disabled="true"/> 
                        <form:input cssClass="form-control form-control-sm d-none" path="Id"  type="text" value="${Usuario.getId()}"/> 
                    </div>

                    <div class="col col-sm-6">
                        Usuario PeOp: <form:input cssClass="form-control form-control-sm"  path="usuarioPeOp" type="text" placeholder="${Usuario.getUsuarioPeOp()}" disabled="true"/>  <br/>
                    </div>
                </div>
                <br/>
                <div class ="row justify-content-end ">
                    <div class="col col-lg-auto">
                        <button type="button" class="btn btn-warning"  onClick='habilitarEdicionUsuario();'>
                             <span id="spinner" class="visually-hidden spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            <span class="visually-hidden">Actualizando datos...</span>
                            Editar datos de Usuario</button>
                    </div>
                </div>

                <div class ="row">
                    <div class="col col-sm-12">
                        <h6>Datos de usuario</h6>
                        <hr/>
                    </div>

                </div>



                <div class ="row">
                    <div class="col col-sm-6">
                        Nombre: <form:input id="nombre" cssClass="form-control" path="nombre" value="${Usuario.getNombre()}" disabled="true" />  
                    </div>
                    <div class="col col-sm-6">
                        Apellidos: <form:input id="apellidos" cssClass="form-control" path="apellidos" value="${Usuario.getApellidos()}" disabled="true"/> 
                    </div>
                    <div class="col col-sm-6">
                        Pais Origen: <form:input id="paisOrigen" cssClass="form-control" path="paisOrigen" value="${Usuario.getPaisOrigen()}" disabled="true"/>  
                    </div>
                    <div class="col col-sm-6">
                        Pais Residencia: <form:input id="paisResidencia" cssClass="form-control" path="paisResidencia" value="${Usuario.getPaisResidencia()}" disabled="true"/>  
                    </div>
                    <div class="col col-sm-6">
                        Edad: <form:input id="fechaNacimiento" cssClass="form-control" path="fechaNacimiento" value="${Usuario.getFechaNacimiento()}" disabled="true"/>  
                    </div>
                    <div class="col col-sm-6">
                        Sexo: <form:input id="Sexo" cssClass="form-control" path="Sexo" value="${Usuario.getSexo()}" disabled="true"/>  
                    </div>
                </div>

                <div class="row justify-content-center mt-3">
                    <div class="col col-md-auto">
                        <button id="actualizaDatos" type="submit" class="btn btn-primary" disabled>
                           
                            Actualizar datos de Usuario
                        </button>
                    </div>

                </div>
            </form:form>

        </div>



        <%@ include file='../Shared/footer.jsp' %>


