<%-- 
    Document   : Recomendar
    Created on : 2 ene. 2021, 21:18:37
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         
        <title>Recomendar - PeOp</title>
        <%@include file='../Shared/styles.jsp'%>
        
   
    </head>
    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <%@include file='../Shared/MenuPrincipal.jsp' %>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <br/>
                    <h4 style="text-align: left">Recomienda PeOp</h4>
                    <hr/>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label for="mailRecomendacion" class="border-bottom">E-mail al que enviar la recomendacion</label>
                    <input type="text" id="mailRecomendacion" placeholder="persona@provedormail.com" class="form-control" name="name">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-9">
                    <label for="texto_mailRecomendacion" class="border-bottom">Contenido del correo</label>
                    <textarea class="form-control" id="texto_mailRecomendacion" rows="3"></textarea>

                </div>
            </div>



            <div class="row text-right">
                <div class="col-9 mt-5">
                    <button class="btn btn-outline-success" onclick="">Recomendar PeOp</button>
                </div>
            </div>



        </div>





        <%@include file='../Shared/footer.jsp' %>
