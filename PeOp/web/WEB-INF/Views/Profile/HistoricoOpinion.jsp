<%-- 
    Document   : HistoricoOpinion
    Created on : 27 dic. 2020, 16:02:46
    Author     : alquimista
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="peop.infraestructure.models.opinionModel"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PeOp - Historico</title>
        <%@include file='../Shared/styles.jsp'%>
    </head>
    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <%@include file='../Shared/MenuPrincipal.jsp' %>
        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col col-12">

                    <h4>Historico de Opiniones</h4>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col col-12">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Nº</th>
                                <th scope="col">Tema</th>
                                <th scope="col">Valoracion</th>
                                <th scope="col">Opinion General</th>
                                <th scope="col">Verdad/Mentira</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int i = 0;
                                List<opinionModel> lst = new ArrayList<>();
                                lst = (List<opinionModel>) request.getAttribute("OpinionesUsuario");
                                for(opinionModel opm :lst) {

                            %>
                            <tr>
                                <th scope="row"><%=i%></th>
                                <td><%=opm.getIdTema()%></td>
                                <td><%=opm.getValoracion()%></td>
                                <td><%=opm.getOpinionGeneral()%></td>
                                <td><%=opm.getVom()%></td>
                                
                            </tr>
                            <%i++;}%>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>




        <%@include file='../Shared/footer.jsp' %>



