

<div class="container-fluid mt-2">
    <hr/>
    <div class="row justify-content-center text-center"> 
        <div class="col col-2">
            <label>
                Powered by:
            </label>
        </div>
    </div>


    <div class="row justify-content-center text-center">  

        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/Java-logo.png" class="w-75" alt="JavaEE">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/spring-logo.png" class="w-50" alt="Spring framework">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/bootstrap-logo.png" class="w-50" alt="BootStrapV5">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/javascript-logo.png" class="w-100" alt="JavaScript">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/Hyperv.png" class="w-100 pt-2" alt="Microsoft Hyper-V">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/sanvalero.png" class="w-100 pt-2" alt="SanValero">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/ionos-logo.png" class="w-100 pt-3" alt="Ionos">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/Tomcat-logo.png" class="w-75" alt="Apache TomCat">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/net-beans.png" class="w-50" alt="NetBeans IDE">
        </div>
        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/gitkraken-icon.png" class="w-50" alt="Git Kraken">
        </div>

        <div class="col col-md-1">
            <img src="${pageContext.request.contextPath}/resources/images/iconos-footer/bitbucket-logo.png" class="w-50" alt="BitBucket">
        </div>
    </div>


</div>
<div class="row justify-content-center"> 
    <div class="col col-11 text-primary">
        <hr/>
    </div>
    <div class="col col-12 mt-3 text-center">
        <label>
            @PeOp 2021 - Version alpha 1.0.5.1
        </label>
    </div>

</div>

<!-- Toast -->
<div class="row justify-content-end">
    <div class="col col-md-auto">
        <div class="toast" id="toastNuevoUsuarioAdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
            <div class="toast-header">
                <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                <strong class="me-auto">PeOp Admin - OK</strong>
                <small>Adminsitracion Interna</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                Usuario creado con �xito.
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-end">
    <div class="col col-sm-auto">
        <div class="toast" id="toastBorrarUsuarioAdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
            <div class="toast-header">
                <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                <strong class="me-auto">PeOp Admin - OK</strong>
                <small>Adminsitracion Interna</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                Usuario borrado con �xito.
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-end">
    <div class="col col-sm-auto">
        <div class="toast" id="toastNuevoPaisAdminOK" role="alert" aria-live="assertive"  aria-atomic="true">
            <div class="toast-header">
                <img src="${pageContext.request.contextPath}/resources/images/shared/ok_verified_s.png" class="rounded me-2" alt="...">
                <strong class="me-auto">PeOp Admin - OK</strong>
                <small>Adminsitracion Interna</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                Nuevo pais creado con �xito.
            </div>
        </div>
    </div>
</div>



<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.esm.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/AjaxCalls.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/opinionsParticles.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Toast.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Charts.js"></script>

<script src="https://kit.fontawesome.com/efd9464ca9.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>  

</body>
</html>

