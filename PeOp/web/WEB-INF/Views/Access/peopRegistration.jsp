<%-- 
    Document   : peopRegistration
    Created on : 19 dic. 2020, 20:15:45
    Author     : alquimista
--%>

<%@page import="peop.libs.ormtool.daos.paises"%>
<%@page import="peop.infraestructure.ORMAccess"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro - PeOp</title>

        <%@include file='../Shared/styles.jsp' %>
       
        
        <script>
               $(function () {
               $('[data-toggle="popover"]').popover();
                });
        </script>
        <script>
            $(document).ready(function() {
            $('.mdb-select').materialSelect();
            });
        </script>
        <script>
           const button = document.querySelector('#button');
           const tooltip = document.querySelector('#tooltip');
           Popper.createPopper(button, tooltip);
        </script>
    </head>

    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
         <form:form action="registrarUsuario" modelAttribute="usuario">
        <div class="container-fluid mt-2">
           

                <h3 class="display-4">Registro en PeOp</h3>
                <p class="lead">Estas a un paso de dejar huella <i class="fas fa-shoe-prints"></i></p>
                <hr/>
                
                <div class="row justify-content-center">
                    <div class="col col-md-4">

                        <label>Nombre</label> <form:input cssClass="form-control" path="nombre"/>  <br/>
                        <label>Apellidos</label> <form:input cssClass="form-control" path="apellidos"/>  <br/>
                        <label>E-mail</label> <form:input type="email" cssClass="form-control" path="email"/> 
                    </div>
                    <div class="col col-md-4" >
                        <label>Usuario PeOp</label> <form:input cssClass="form-control" path="usuarioPeOp"/>  <br/>
                        <label>Establece la contraseña</label> <form:input type="password" cssClass="form-control" path="userPass"/>  <br/>
                       
                        <label>Fecha Nacimiento</label> 
                        <form:input cssClass="form-control" type="date" path="fechaNacimiento" id="datetime1" name="datetime1"/>  <br/>
                        <label>Sexo</label><br/>
                        
                            <div class="col col-md-auto">
                                <form:radiobutton cssClass="form-check-input" path="sexo" value="Hombre"/>Hombre
                                <form:radiobutton cssClass="form-check-input" path="sexo" value="Mujer"/>Mujer
                                <form:radiobutton cssClass="form-check-input" path="sexo" value="Alternativo"/>Alternativo
                            </div>

                        
                    </div>

                </div>

                <div class="row justify-content-center m-auto">

                    <div class="col col-sm-4">
                        Nacionalidad
                        <form:select path="nacionalidad" cssClass="form-select" searchable="Search here..">
                            <%
                            List<String> lstNacionalidades = (List<String>) request.getAttribute("lstNacionalidades");
                            String n= "";
                                for(String nacionalidad: lstNacionalidades) {
                                    n = nacionalidad;
                            %>
                            <form:option data-tokens="<%=n%>" value="<%=n%>"></form:option>
                            <%}%>
                        </form:select>
                    </div>
                    <div class="col col-sm-4">
                        Pais de Residencia 
                        <form:select path="paisResidencia" cssClass="form-select" data-live-search="true">
                            <%
                              List<String> lstPaisesR = (List<String>) request.getAttribute("lstPaises");
                              String pr = "";
                                  for(String pais: lstPaisesR) {
                                      pr = pais;
                            %>

                            <form:option data-tokens="<%=pr%>" value="<%=pr%>"></form:option>
                            <%}%>
                        </form:select>
                    </div>


                    <div class="col col-sm-4">
                        Pais de Origen 
                        <form:select path="paisOrigen" cssClass="form-select" data-live-search="true">
                            <%
                          List<String> lstPaisesO = (List<String>) request.getAttribute("lstPaises");
                          String po = "";
                              for(String pais: lstPaisesO) {
                                  po = pais;
                            %>
                            <form:option data-tokens="<%=po%>" value="<%=po%>"></form:option>
                            <%}%>
                        </form:select>
                    </div>

                </div>
                <hr/>
                <div class="row justify-content-center">
                    <div class="col-sm-1">
                        <input type="submit" class="btn btn-outline-success" value="Registrarse">
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-outline-danger" role="button" href="/PeOp">Cancelar</a>
                    </div>
                </div>
           
        </div>
 </form:form>
        <%@include file='../Shared/footer.jsp' %>

