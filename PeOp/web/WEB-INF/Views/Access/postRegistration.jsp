<%-- 
    Document   : postLogin
    Created on : 6 dic. 2020, 22:40:00
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenido a PeOp</title>
        <%@include file='../Shared/styles.jsp' %>
    </head>
    <body>
        <div class="container-fluid mt-2">
            <div class="row ">
                <h1>Usuario registrado correctamente!</h1>
                <hr/>
            </div>
            <div class="row ">
                <div class="col col-sm-12">
                    <p class="text-center">
                        Bienvenido a PeOp<br/>
                        Se ha registrado correctamente en PeOp. <br/>
                        Para poder empezar a usar la plataforma primero tiene que validar su direccion de correo electrónico. <br/>
                        Para ello le hemos enviado un email con un enlace que realizará activacion de su perfil.
                        ${successLogin}
                    </p>
                </div>

            </div>
            <div class="row text-center">
                <div class="col col-sm-12">
                    <a class="btn btn-primary" href="../Profile/verPerfil"> Ver Perfil</a>
                    <a class="btn btn-primary" href="../Tools/verOpiniones"> Ver Opiniones</a>
                    <a class="btn btn-primary" href="../Index"> Ir al Inicio</a>
                </div>

            </div>
        </div>





        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.esm.js"></script>
        <script src="https://kit.fontawesome.com/efd9464ca9.js" crossorigin="anonymous"></script>
    </body>
</html>
