
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login - PeOp</title>
        <%@include file='../Shared/styles.jsp' %>

    </head>
    <body>
        <form action = "IndexLogin" method="post">
            <div class="container-fluid">

                <div class="row justify-content-md-center">
                    <div class="col col-md-auto">
                        <img alt="PeOp-logo" src="${pageContext.request.contextPath}/resources/images/PeOpLogo.png">
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col col-md-6  text-center">
                        <label for="usuario" class="col col-sm-3 col-form-label ">
                            Usuario
                        </label>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col col-md-3 justify-content-center">
                        <input type="text" class="form-control" id="usuario" name="nombreUsuario">
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col col-md-6 text-center">
                        <label for="inputPassword" class=" col col-sm-3 col-form-label">Contraseña</label>

                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col col-md-3 text-center">
                        <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="passUsuario">
                    </div>
                </div>

                <div class="row justify-content-center mt-3">
                    <div class="col col-sm-1  ">
                        <input class="btn btn-outline-primary" type="submit" value="Acceder">
                    </div>

                    <div class="col col-sm-1">
                        <a class="btn btn-outline-info" href="Access/peopRegistration">Registrase</a>
                    </div>
                </div>

            </div>
        </form>

        <%@include file='../Shared/footer.jsp' %>