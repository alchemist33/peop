<%-- 
    Document   : TemasOpinion
    Created on : 13 dic. 2020, 19:05:59
    Author     : alquimista
--%>

<%@page import="peop.infraestructure.models.temasOpinionModel"%>
<%@page import="peop.infraestructure.models.SubcategoriasTemasModel"%>
<%@page import="peop.infraestructure.models.categoriasTemasModel"%>
<%@page import="peop.libs.poolconnection.dao.CategoriasTemas"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="peop.libs.ormtool.daos.TemasOpinion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Temas para Opinar</title>
        <%@include file='../Shared/styles.jsp'%>
        <script>
        function OcultarMostrarSubCategorias(div) {

            var y = document.querySelectorAll('#contenedorSubCategorias .list-group-item');
            var id = "";
            var divId = "";

            for (i = 0; i < y.length; i++) {

               var x = y[i];
               id = new String(x.className.substring(16));
               divId = new String(div);

               if(id.trim() !== divId.trim()){
                x.style.display = "none";
               }else {
                x.style.display = "block";
               }
             }       
           }

        function OcultarMostrarTematicas(div) {

            var z = document.querySelectorAll('#contenedorTematicas .list-group-item');
            var idSub = "";
            var divIdSub = "";
            
          for(i = 0;i < z.length;i++) {

               var x = z[i];
               idSub = new String(x.className.substring(16));
               divIdSub = new String(div);

               if(idSub.trim() !== divIdSub.trim()){
                x.style.display = "none";
               }else {
                x.style.display = "block";
               }
            }
        }
        </script>

        <script>
    function seleccionaSubCategorias(){

        $.ajax({
            url: 'Tools/getSubCategoria',
            success: function(respuesta) {
            
                console.log(respuesta);
                var acordeon = $("#acordion");
        acordeon.append('');	
            },
            error: function() {
            alert("error");
        console.log("No se ha podido obtener la información");
            }
        });
    }
        </script>
    </head>


    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">

        <!-- Menu Principal superior -->
        <%@include file='../Shared/MenuPrincipal.jsp' %>




        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col col-12">
                    <h4>Selecciona un tema sobre el que opinar</h4>
                    <hr/>
                </div>
            </div>
            <form:form method="POST" action="Opinar" modelAttribute="temaOpinion">
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                Categorias
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <div class="list-group-flush">

                                    <div class="list-group-item">
                                        <table class="table">
                                            <%
                                        List<categoriasTemasModel> lstOK = (List<categoriasTemasModel>) request.getAttribute("categoriasTemas");
                                        for(categoriasTemasModel cat: lstOK) {
                                            String FullnombreCategoria  = cat.getNombreCategoria() + "," +cat.getIdCategoria();
                                            String nombreCategoria  = cat.getNombreCategoria();
                                            String funcion  = "OcultarMostrarSubCategorias('"+ cat.getIdCategoria()+" ')";
                                            %>
                                            <tr>
                                                <td>
                                                    <p class="mb-0"> 
                                                        <i class="<%out.write("fas fa-laptop mr-4 pr-3");%>" aria-hidden="true"></i>
                                                        <form:radiobutton cssClass="btn btn-link" 
                                                                          path="NombreCategoria" 
                                                                          data-bs-toggle="collapse" 
                                                                          data-bs-target="#flush-collapseTwo" 
                                                                          aria-controls="flush-collapseOne" 
                                                                          aria-expanded="true"
                                                        id="<%=cat.getIdCategoria()%>"
                                                        value="<%=FullnombreCategoria%>"
                                                        onchange="<%=funcion.toString()%>"/> <%out.write(nombreCategoria);%>
                                                    </p>
                                                </td>
                                            </tr>
                                            <%}%>
                                        </table>
                                    </div>

                                </div>   
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                SubCategorias
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <div class="list-group-flush" id="contenedorSubCategorias">
                                    <%
                                         List<SubcategoriasTemasModel> lst2OK = (List<SubcategoriasTemasModel>) request.getAttribute("SubcategoriasTemas");
                                        for(SubcategoriasTemasModel scat: lst2OK) {
                                            String FullnombreSubCategoria  = scat.getNombreSubCategoria() +","+ scat.getIdSubCategoria();
                                            String nombreSubCategoria  = scat.getNombreSubCategoria();
                                            String IdCategoria  = scat.getIdCategoria();
                                            String funcionTemas  = "OcultarMostrarTematicas('"+ scat.getIdSubCategoria()+" ')";
                                    %>
                                    <div class="list-group-item <%=IdCategoria%>" >
                                        <table>
                                            <tr>
                                                <td>
                                                    <p class="mb-0">
                                                        <i class="<%out.write("fas fa-laptop mr-4 pr-3");%>" aria-hidden="true"></i>
                                                        <form:radiobutton cssClass="btn btn-link" 
                                                                          path="NombreSubCategoria" 
                                                                          data-bs-toggle="collapse" 
                                                                          data-bs-target="#flush-collapseThree" 
                                                                          aria-controls="flush-collapseTwo" 
                                                                          aria-expanded="true"
                                                        id="<%=IdCategoria%>"
                                                        value="<%=FullnombreSubCategoria%>"
                                                        onchange="<%=funcionTemas.toString()%>"/> <%out.write(nombreSubCategoria);%>
                                                    </p>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                    <%}%>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                Tematicas
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <div class="list-group-flush" id=contenedorTematicas>
                                    <%
                                         List<temasOpinionModel> lst3OK = (List<temasOpinionModel>) request.getAttribute("TemasOpinion");
                                        for(temasOpinionModel tema: lst3OK) {
                                            String FullNombreTema  = tema.getNombreTema() + "," +tema.getIdTema();
                                            String nombreTema  = tema.getNombreTema();
                                            String IdTema  = tema.getIdTema();
                                            String IdSubCategoria  = tema.getIdSubCategoriaTema();
                                    
                                    %>
                                    <div class="list-group-item <%=IdSubCategoria%>">
                                        <table >
                                            <tr>
                                                <td>
                                                    <p class="mb-0">
                                                        <i class="<%out.write("fas fa-laptop mr-4 pr-3");%>" aria-hidden="true"></i>
                                                        <form:radiobutton 
                                                            cssClass="btn btn-link" 
                                                            path="NombreTema" 
                                                        id="<%=IdTema%>"
                                                        value="<%=FullNombreTema%>"/> <%out.write(nombreTema);%>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <%}%>
                                </div>                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center mt-3">
                    <div class="col col-md-auto">
                        <input type="submit" class="btn btn-primary" value="Opinar sobre esto">
                    </div>

                </div>
            </form:form>
        </div>


        <!-- Lista Principal Temas Opinion 
            <div class="container-fluid">
    
        <form:form method="POST" action="Opinar" modelAttribute="temaOpinion">
            <div id="accordion">
                <div class="card text-center">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <label class="text-sm-center">
                                Categorias  <span class="badge badge-info">4</span>
                            </label>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <div class="list-group-flush">
            <%
                List<categoriasTemasModel> lst = (List<categoriasTemasModel>) request.getAttribute("categoriasTemas");
                for(categoriasTemasModel cat: lst) {
                    String FullnombreCategoria  = cat.getNombreCategoria() + "," +cat.getIdCategoria();
                    String nombreCategoria  = cat.getNombreCategoria();
                    String funcion  = "OcultarMostrarSubCategorias('"+ cat.getIdCategoria()+" ')";
            %>
            <div class="list-group-item">
                <table>
                    <tr>
                        <td>
                            <p class="mb-0"> 
                                <i class="<%out.write("fas fa-laptop mr-4 pr-3");%>" aria-hidden="true"></i>
            <form:radiobutton cssClass="btn btn-link" 
                              path="NombreCategoria" 
                              data-toggle="collapse" 
                              data-target="#collapseTwo" 
                              aria-controls="collapseOne" 
                              aria-expanded="true"
            id="<%=cat.getIdCategoria()%>"
            value="<%=FullnombreCategoria%>"
            onchange="<%=funcion.toString()%>"/> <%out.write(nombreCategoria);%>
        </p>
    </td>
</tr>
</table>
</div>
            <%}%>
        </div>
    </div>
</div>
</div>
<div class="card text-center">
<div class="card-header" id="headingTwo">
    <h5 class="mb-0">
        <label class="text-sm-center">
            SubCategorias  <span class="badge badge-info">4</span>
        </label>                                    
    </h5>
</div>
<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
    <div class="card-body">
        <div class="list-group-flush" id="contenedorSubCategorias">
            <%
                 List<SubcategoriasTemasModel> lst2 = (List<SubcategoriasTemasModel>) request.getAttribute("SubcategoriasTemas");
                for(SubcategoriasTemasModel scat: lst2) {
                    String FullnombreSubCategoria  = scat.getNombreSubCategoria() +","+ scat.getIdSubCategoria();
                    String nombreSubCategoria  = scat.getNombreSubCategoria();
                    String IdCategoria  = scat.getIdCategoria();
                    String funcionTemas  = "OcultarMostrarTematicas('"+ scat.getIdSubCategoria()+" ')";
            %>
            <div class="list-group-item <%=IdCategoria%>" >
                <table>
                    <tr>
                        <td>
                            <p class="mb-0">
                                <i class="<%out.write("fas fa-laptop mr-4 pr-3");%>" aria-hidden="true"></i>
            <form:radiobutton cssClass="btn btn-link" 
                              path="NombreSubCategoria" 
                              data-toggle="collapse" 
                              data-target="#collapseThree" 
                              aria-controls="collapseTwo" 
                              aria-expanded="true"
            id="<%=IdCategoria%>"
            value="<%=FullnombreSubCategoria%>"
            onchange="<%=funcionTemas.toString()%>"/> <%out.write(nombreSubCategoria);%>
        </p>
    </td>

</tr>
</table>
</div>
            <%}%>
        </div>
    </div>
    <span class="badge badge-pill badge-primary btn mb-3" data-toggle="collapse" data-target="#collapseOne"  aria-controls="collapseTwo">
        Atras
    </span>
</div>
</div>
<div class="card text-center">
<div class="card-header" id="headingThree">
    <h5 class="mb-0">
        <label class="text-sm-center">
            Tematicas  <span class="badge badge-info">4</span>
        </label>                                    
    </h5>
</div>
<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
    <div class="card-body">
        <div class="list-group-flush" id=contenedorTematicas>
            <%
                 List<temasOpinionModel> lst3 = (List<temasOpinionModel>) request.getAttribute("TemasOpinion");
                for(temasOpinionModel tema: lst3) {
                    String FullNombreTema  = tema.getNombreTema() + "," +tema.getIdTema();
                    String nombreTema  = tema.getNombreTema();
                    String IdTema  = tema.getIdTema();
                    String IdSubCategoria  = tema.getIdSubCategoriaTema();
                                    
            %>
            <div class="list-group-item <%=IdSubCategoria%>">
                <table >
                    <tr>
                        <td>
                            <p class="mb-0">
                                <i class="<%out.write("fas fa-laptop mr-4 pr-3");%>" aria-hidden="true"></i>
            <form:radiobutton 
                cssClass="btn btn-link" 
                path="NombreTema" 
            id="<%=IdTema%>"
            value="<%=FullNombreTema%>"/> <%out.write(nombreTema);%>
        </p>
    </td>
</tr>
</table>
</div>
            <%}%>
        </div>
        <br/>
    </div>
    <span class=" badge badge-pill badge-primary btn mb-3" data-toggle="collapse" data-target="#collapseTwo"  aria-controls="collapseThree">
        Atras
    </span>
</div>
</div> 
</div>


        </form:form>
    </div>
        -->


        <!-- Footer -->                    
        <%@ include file='../Shared/footer.jsp' %>


