<%-- 
    Document   : opinionRegistrada
    Created on : 13 dic. 2020, 21:55:20
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Opinion Registrada</title>
        <%@include file='../Shared/styles.jsp'%>
   
    </head>
    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <%@include file='../Shared/MenuPrincipal.jsp' %>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <br/>
                    <h4 style="text-align: left">Opinion registrada con exito</h4>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p style="text-align: center">Tu valoracion ha sido <strong>${opinion.valoracion}</strong></p>
                    <p style="text-align: center">Tu valoracion ha sido <strong>${opinion.vom}</strong></p>
                </div>
            </div>

            <br/>
            <div class="row justify-content-center">
                <div class="col-6">
                    <!--* Card init *-->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <!-- Title -->
                            <h5 class="h5 mb-0">Otras opiniones sobre este tema</h5>
                        </div>
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="chart">
                                <!-- Chart wrapper -->
                                <canvas id="myChart" class="chart-canvas"></canvas>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-6">
                    <!--* Card init *-->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <!-- Title -->
                            <h5 class="h5 mb-0">Cantidad de opiniones sobre este tema</h5>
                        </div>
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="chart">
                                <!-- Chart wrapper -->
                                <canvas id="myChart2" class="chart-canvas"></canvas>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row justify-content-center mt-3">
                <div class="col-12">
                    <div class="text-center">
                        <a class="btn btn-outline-primary" role="button" href="TemasOpinion"> Volver a Opinar</a>
                        <a class="btn btn-outline-primary" role="button" href="../Profile/verPerfil"> Ver Perfil</a>
                        <a class="btn btn-outline-primary" role="button" href="verOpiniones"> Ver Opiniones</a>
                        <a class="btn btn-outline-primary" role="button" href="../Index"> Ir al Inicio</a>
                    </div>
                </div>
            </div>
        </div>
                

    
        <%@include file='../Shared/footer.jsp' %>






