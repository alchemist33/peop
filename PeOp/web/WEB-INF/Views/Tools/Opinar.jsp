<%-- 
    Document   : Opinar
    Created on : 13 dic. 2020, 19:05:22
    Author     : alquimista
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="peop.infraestructure.models.options_OpinionGeneralModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Opinar - PeOp</title>
        <%@include file='../Shared/styles.jsp'%>

    </head>
    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <%@include file='../Shared/MenuPrincipal.jsp'%>

        <form:form action="procesarOpinion" modelAttribute="opinion">
            <div class="container-fluid">
                <h4 class="text-left mt-3">Dar mi opinion sobre ${categoriaTemaEscogido.nombreTema}</h4>
                <hr/>

                <div class="row justify-content-center ">
                    <div class="col-md-2 pl-0 ml-5 mt-3 text-right">
                        <label for="nombreCategoria">Categoria seleccionada:</label>
                    </div>
                    <div class="col-md-4 mt-2 pl-0">
                        <input class="form-control text-secondary" type="text" id="nombreCategoria" placeholder="${categoriaTemaEscogido.nombreCategoria}"   readonly> 
                        <form:input path="IdCategoria" cssClass="form-control d-none" type="text" id="IdCategoria" value="${categoriaTemaEscogido.idCategoria}" />
                    </div>
                </div>   
                <div class="row justify-content-center">
                    <div class="col-md-2 pl-0 ml-5 mt-3 text-right">
                        <label for="subcategoria">SubCategoria seleccionada:</label>
                    </div>
                    <div class="col-md-4 mt-2 pl-0">
                        <input class="form-control text-secondary" type="text" id="subcategoria" placeholder="${categoriaTemaEscogido.nombreSubCategoria}"  readonly>  
                        <form:input path="IdSubCategoria" cssClass="form-control d-none" type="text" id="IdSubCategoria" value="${categoriaTemaEscogido.idSubCategoria}" />
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-2 pl-0 ml-5 mt-3 text-right">
                        <label for="tema">Tema seleccionado:</label>
                    </div>
                    <div class="col-md-4 mt-2 pl-0">
                        <input class="form-control text-secondary" type="text" id="tema" placeholder="${categoriaTemaEscogido.nombreTema}" readonly>  
                        <form:input path="IdTema" cssClass="form-control d-none" type="text" id="IdSubCategoria" value="${categoriaTemaEscogido.idTema}" />
                    </div>
                </div>

                <div class="row justify-content-center mt-3">
                    <div class="col-md-6">
                        <label class="text-info border-bottom mb-2">
                            Opinion general: 
                        </label>

                        <div class="form-check mb-2">
                            <%   
                            List<options_OpinionGeneralModel> lstOptions =  new ArrayList<>();
                            lstOptions = (List<options_OpinionGeneralModel>) request.getAttribute("lstOpciones");
                            for(options_OpinionGeneralModel option :lstOptions) {
                                String op = option.getOpcion();
                            %>
                            <div class="col-md-4">
                                <form:radiobutton cssClass="form-check-input" path="opinionGeneral" label="<%=op%>" value="<%=op%>"/>
                            </div>
                            <%}%>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <label class="text-info border-bottom mb-2">
                            Crees que es verdad o mentira:
                        </label> <br/>
                        <div class="form-check mb-2">
                            <div class="col-md-3">
                                <form:radiobutton cssClass="form-check-input" path="vom" label="Mentira" value="Mentira"/> 
                            </div>
                            <div class="col-md-3">
                                <form:radiobutton cssClass="form-check-input" path="vom" label="Verdad" value="Verdad"/>
                            </div>
                        </div>
                        Valoracion: <form:input cssClass="form-control" path="valoracion"/> <form:errors path="valoracion" style="color:red"/><br/> 
                    </div>
                </div>
                <div class="row justify-content-center text-center">
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-outline-success" value="Enviar Opinion">
                        <a class="btn btn-outline-info" role="button" href="TemasOpinion">Atras</a>
                    </div>
                </div>
            </div>

            <!-- 
                 Valoracion: <form:input path="valoracion"/> <br/>
                 Valoracion: <form:input path="valoracion"/> <br/>
                 Valoracion: <form:input path="valoracion"/> <br/> 
                 Valoracion: <form:input path="valoracion"/> <br/> 
            -->

        </form:form>

        <%@include file='../Shared/footer.jsp' %>


