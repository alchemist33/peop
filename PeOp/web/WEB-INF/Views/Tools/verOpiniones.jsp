<%-- 
    Document   : verOpiniones
    Created on : 27 dic. 2020, 16:02:12
    Author     : alquimista
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- <meta http-equiv="refresh"  content="5" /> -->
        <title>Ver Opiniones - PeOp</title>
        <%@include file='../Shared/styles.jsp' %>


    </head>

    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <!-- Menu Principal superior -->
        <%@include file='../Shared/MenuPrincipal.jsp' %>
        <!-- Menu Principal superior -->

        <div class="container-fluid">

            <div class="row">
                <div class="col col-12 mt-2">
                    <h4>Ver Opiniones</h4>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col col-sm-3">
                    <label>Residentes en:</label>
                    <select  class="form-select" id="filtro_paisResidencia">
                        <%
                        List<String> lstPaies2 = (List<String>) request.getAttribute("lstP");
                        String p= "";
                            for(String pa:lstPaies2) {
                                p = pa;
                        %>
                        <option value="<%=p%>" label="<%=p%>"></option>
                        <%}%>
                    </select>
                </div>
                <div class="col col-sm-3">
                    <label>Nacidos en:</label>
                    <select  class="form-select" id="filtro_paisOrigen">
                        <%
                        List<String> lstPaies3 = (List<String>) request.getAttribute("lstPaises2");
                        String p3= "";
                            for(String pais3:lstPaies3) {
                                p3 = pais3;
                        %>
                        <option value="<%=p3%>" label="<%=p3%>"></option>
                        <%}%>
                    </select>
                </div>
                <div class="col col-sm-3">
                    <label>Edad:</label>
                    <input class="form-control" type="date" id="filtro_fechaN" name="datetime1"/>                                   
                </div>

                <div class="col col-sm-3">
                    <label>Sexo:</label>
                    <select  class="form-select" id="filtro_sexo">

                        <option value="Hombre" label="Hombre"></option>
                        <option value="Mujer" label="Mujer"></option>
                        <option value="Alternativo" label="Alternativo"></option>

                    </select>
                </div>
            </div>

            <div class="row justify-content-center mt-3">
                <div class="col col-12 text-center">
                    <button class="btn btn-outline-success" onclick="verOpiniones()"><i class="fas fa-search mr-1"></i>Ver Opinines</button>
                    
                </div>
            </div>

        </div>


        <hr/>
        <div class="container-fluid">
            <div class ="row">

                <nav class="nav fill justify-content-center">
                    <div class="nav nav-tabs " id="nav-tab" role="tablist">
                        
                        <a class="nav-link active" id="nav-list-tab" data-bs-toggle="tab" href="#nav-list" role="tab" aria-controls="nav-list" aria-selected="true">
                            <i class="fas fa-list mr-2"></i>
                            Lista 
                        </a> 
                        
                        <a class="nav-link" id="nav-panel-tab" data-bs-toggle="tab" href="#nav-panel" role="tab" aria-controls="nav-panel" aria-selected="false">
                            <i class="fas fa-th mr-2"></i>
                            Panel 
                        </a> 
                        
                        <a class="nav-link" id="nav-particulas-tab" data-bs-toggle="tab" href="#nav-particulas" role="tab" aria-controls="nav-particulas" aria-selected="false">
                            <i class="fas fa-braille mr-2"></i>
                            Particulas
                        </a>

                    </div>
                </nav>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-list" role="tabpanel" aria-labelledby="nav-list-tab">  <%@include file='ListView.jsp'%></div> 
                <div class="tab-pane fade" id="nav-panel" role="tabpanel" aria-labelledby="nav-panel-tab">  <%@include file='panelView.jsp'%></div>
                <div class="tab-pane fade" id="nav-particulas" role="tabpanel" aria-labelledby="nav-particulas-tab">  <%@include file='particlesView.jsp'%></div> 

            </div>
        </div>

        <%@ include file='../Shared/footer.jsp' %>