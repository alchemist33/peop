
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PeOp (People Opinions)</title>
        <%@include file='Shared/styles.jsp' %>

    </head>

    <body style="background-image:url(${pageContext.request.contextPath}/resources/images/shared/cutcube.png)">
        <!-- Menu Principal superior -->
        <div class="container-fluid">

            <nav class="navbar navbar-expand-lg navbar-dark bg-primary biselado">
                <div class="container-fluid">
                    <a class="navbar-brand" href="">PeOp</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="Index">Inicio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="Tools/verOpiniones">Ver Opiniones</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="Tools/TemasOpinion">Opinar</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="Admin/Administrar" tabindex="-1" aria-disabled="true">Administrar</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-user mr-1"></i>
                                    <label class="ml-1">
                                        Cuenta
                                    </label>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="Profile/verPerfil">Perfil</a></li>
                                    <li><a class="dropdown-item" href="Profile/HistoricoOpinion">Historial</a></li>
                                    <li><div class="dropdown-divider"></div></li>
                                    <li><a class="dropdown-item" href="Profile/Recomendar">Recomendar</a></li>
                                    <li><div class="dropdown-divider"></div></li>
                                    <li> <a class="dropdown-item" href="../PeOp">Cerrar sesion</a></li>
                                </ul>
                            </li>
                        </ul>
                        <form class="d-flex">
                            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-dark" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>

        </div>


        <div class="container-fluid mt-2">

            <h1 class="display-4">PeOp - People Opinions</h1>
            <p class="lead">La mayor plataforma de opiniones y encuestas a nivel mundial. Que el mundo conozca tu opinión.</p>


        </div>

        <div class="container-fluid">
            <div id="carouselExampleDark" class="carousel carousel-dark slide w-50 h-50 m-auto" data-bs-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"></li>
                    <li data-bs-target="#carouselExampleDark" data-bs-slide-to="1"></li>
                    <li data-bs-target="#carouselExampleDark" data-bs-slide-to="2"></li>
                    <li data-bs-target="#carouselExampleDark" data-bs-slide-to="3"></li>
                    <li data-bs-target="#carouselExampleDark" data-bs-slide-to="4"></li>
                </ol>
                <div class="carousel-inner border border-primary rounded-3">
                    <div class="carousel-item active " data-bs-interval="10000">
                        <img src="${pageContext.request.contextPath}/resources/images/portada/shubhangee-vyas-MQXXPYJX3Cc-unsplash.jpg" class="d-block w-100 m-auto" alt="...">
                        <div class="carousel-caption d-none d-md-block bg-gradient border">
                            <h5 class="text-light ">El mundo opina</h5>
                            <p class="text-light">Analiza opiniones del mundo entero y descubre cosas sorprendentes</p>
                        </div>
                    </div>
                    <div class="carousel-item" data-bs-interval="2000">
                        <img src="${pageContext.request.contextPath}/resources/images/portada/priscilla-du-preez-VTE4SN2I9s0-unsplash.jpg" class="d-block w-100  m-auto" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Second slide label</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="${pageContext.request.contextPath}/resources/images/portada/macau-photo-agency-1EUmODQBG8g-unsplash.jpg" class="d-block w-100 m-auto" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Third slide label</h5>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="${pageContext.request.contextPath}/resources/images/portada/allgo-an-app-for-plus-size-people-OxwQT1woe-Y-unsplash.jpg" class="d-block w-100 m-auto" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Cuarto slide label</h5>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="${pageContext.request.contextPath}/resources/images/portada/yuvraj-singh-wZYXer0DwUE-unsplash.jpg" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Quinto slide label</h5>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleDark" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleDark" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </a>
            </div>
        </div>
        <div class="container-md mt-4">
            <div class="row">
                <div class="col-sm-4">
                    <div class="card border border-primary rounded-3 bg-light bg-gradient">
                        <div class="card-body ">
                            <h5 class="card-title text-dark">Opiniones del mundo <i class="fas fa-globe pl-2"></i></h5>  
                            <p class="card-text">En PeOp podrás visualizar las opiniones de cualquier parte del mundo.</p>
                            <a href="#" class="btn btn-primary">Ver Opiniones</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card border border-primary rounded-3 bg-light bg-gradient">
                        <div class="card-body ">
                            <h5 class="card-title text-dark ">Deja huella<i class="fas fa-shoe-prints pl-2 "></i></h5>
                            <p class="card-text">Nuestras opiniones son lo mas importante para construir nuevos cambios</p>
                            <a href="#" class="btn btn-primary">Opinar</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card border border-primary rounded-3 bg-light bg-gradient">
                        <div class="card-body ">
                            <h5 class="card-title text-dark ">Une a los tuyos<i class="fas fa-users pl-2"></i></h5>
                            <p class="card-text">Recomienda a tus familiares, amigos, conocidos... todas las opiniones importan</p>
                            <a href="#" class="btn btn-primary">Recomendar PeOp</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file='Shared/footer.jsp' %>



       




