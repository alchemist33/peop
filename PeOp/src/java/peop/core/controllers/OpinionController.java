/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.core.controllers;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import peop.infraestructure.PoolConnectionAccess;
import peop.infraestructure.models.SeleccionTemaModel;
import peop.infraestructure.models.SubcategoriasTemasModel;
import peop.infraestructure.models.categoriasTemasModel;
import peop.infraestructure.models.opinionModel;
import peop.infraestructure.models.options_OpinionGeneralModel;
import peop.infraestructure.models.temasOpinionModel;
import peop.infraestructure.models.usuariosModel;

import sun.font.TrueTypeFont;

/**
 *
 * @author alquimista
 */
@Controller
public class OpinionController {

    @InitBinder
    public void miBinder(WebDataBinder binder) {

        StringTrimmerEditor recortaEspacios = new StringTrimmerEditor(true);
        binder.registerCustomEditor(String.class, recortaEspacios);

    }

    @RequestMapping("/Tools/TemasOpinion")
    public String muestraTemas(@ModelAttribute("temaOpinion") SeleccionTemaModel stm, Model modelo) {
        //ORMCore ormc = new ORMCore();
        PoolConnectionAccess pca = new PoolConnectionAccess();

        List<categoriasTemasModel> catsTModel = pca.getCategorias();
        List<SubcategoriasTemasModel> SubcatsTModel = pca.getSubCategorias();
        List<temasOpinionModel> temasOpsModel = pca.getTemasOpinion();

        modelo.addAttribute("categoriasTemas", catsTModel);
        modelo.addAttribute("SubcategoriasTemas", SubcatsTModel);
        modelo.addAttribute("TemasOpinion", temasOpsModel);

        return "Tools/TemasOpinion";
    }

    @RequestMapping("/Tools/Opinar")
    public String seleccionTemaOpinion(@ModelAttribute("temaOpinion") SeleccionTemaModel stm, Model modelo) {
        PoolConnectionAccess pca = new PoolConnectionAccess();

        opinionModel op = new opinionModel();

        List<options_OpinionGeneralModel> opciones_OpinionGeneral = new ArrayList<>();
        opciones_OpinionGeneral = pca.getoptions_OpinionGeneral();

        String[] idCatNomCat = stm.getNombreCategoria().split(",");
        stm.setNombreCategoria(idCatNomCat[0]);
        stm.setIdCategoria(idCatNomCat[1]);

        String[] idSubCatNomSubCat = stm.getNombreSubCategoria().split(",");
        stm.setNombreSubCategoria(idSubCatNomSubCat[0]);
        stm.setIdSubCategoria(idSubCatNomSubCat[1]);

        String[] idTemaNomTema = stm.getNombreTema().split(",");
        stm.setNombreTema(idTemaNomTema[0]);
        stm.setIdTema(idTemaNomTema[1]);

        modelo.addAttribute("categoriaTemaEscogido", stm);
        modelo.addAttribute("opinion", op);
        modelo.addAttribute("lstOpciones", opciones_OpinionGeneral);

        return "Tools/Opinar";
    }

    @RequestMapping("/Tools/procesarOpinion")
    public String procesarOpinion(@Valid @ModelAttribute("opinion") opinionModel op, BindingResult resultadoValidacion) {
        PoolConnectionAccess pca = new PoolConnectionAccess();
        op.setIdUsuario(ProfileController.logedUser.getId());
        if (resultadoValidacion.hasErrors()) {

            return "Tools/Opinar";
        } else {

            pca.nuevaOpinion(op);
            return "Tools/opinionRegistrada";
        }
    }

    @RequestMapping("/Tools/opinionRegistrada")
    public String opinionRegistrada(Model modelo) {

        opinionModel op = new opinionModel();
        modelo.addAttribute("opinion", op);

        return "Tools/opinionRegistrada";
    }

    @RequestMapping("/Tools/verOpiniones")
    public String muestraVerOpiniones(@ModelAttribute("searchparams") usuariosModel um, Model model) {

        PoolConnectionAccess pca = new PoolConnectionAccess();

        List<String> paises = new ArrayList<>();
        List<String> paises2 = new ArrayList<>();
        List<String> nacionalidades = new ArrayList<>();
        pca.getPaises().forEach(pais -> {
            paises.add(pais.getNombrePais());
            paises2.add(pais.getNombrePais());
            nacionalidades.add(pais.getNacionalidad());
        });

        model.addAttribute("lstP", paises);
        model.addAttribute("lstPaises2", paises2);

        return "Tools/verOpiniones";
    }

    @RequestMapping("/Tools/ListarOpiniones")
    public String ListarOpiniones(@ModelAttribute("searchparams") usuariosModel opinionModel) {

        return "Tools/verOpiniones";
    }

//    @RequestMapping("/Tools/getOpinionsPanelView")
//    public String getOpinionsPanelView(@ModelAttribute("searchparams") usuariosModel um2, Model modelo) {
//
//        PoolConnectionAccess pca = new PoolConnectionAccess();
//
//        List<String> paises = new ArrayList<>();
//        List<String> paises2 = new ArrayList<>();
//        List<String> nacionalidades = new ArrayList<>();
//        pca.getPaises().forEach(pais -> {
//            paises.add(pais.getNombrePais());
//            paises2.add(pais.getNombrePais());
//            nacionalidades.add(pais.getNacionalidad());
//        });
//
//        modelo.addAttribute("lstP", paises);
//        modelo.addAttribute("lstPaises2", paises2);
//
//        String IdTema = "";
//        String nacionalidad = um2.getNacionalidad();
//        String paisResidencia = um2.getPaisResidencia();
//        String paisOrigen = um2.getPaisOrigen();
//        String fechaNacimiento = um2.getFechaNacimiento().toString();
//
//        List<opinionModel> opinionsPanelView = pca.getOpinionesFiltro(IdTema, nacionalidad, paisResidencia, paisOrigen, fechaNacimiento);
//
//        modelo.addAttribute("opsPanelView", opinionsPanelView);
//        return "Tools/verOpiniones";
//
//    }
    //Respuestas Ajax
    @ResponseBody
    @RequestMapping(value = "Tools/AjaxCall")
    public void getSubCategorias(@RequestBody SubcategoriasTemasModel search, Model modelo) {

        PoolConnectionAccess pca = new PoolConnectionAccess();
        List<SubcategoriasTemasModel> SubcatsTModel = pca.getSubCategorias();

        modelo.addAttribute("SubcategoriasTemas", SubcatsTModel);

    }

    @ResponseBody
    @RequestMapping(value = "Tools/verOpinionesFiltro")
    public List<String> getOpinionesFiltro(@RequestBody String request, Model modelo) {

        PoolConnectionAccess pca = new PoolConnectionAccess();
        String[] filterOptions = request.split("&");
        // List<opinionModel> opiniones = pca.getOpinionesFiltro(filterOptions);
        List<peop.libs.poolconnection.dao.opinion> lstOpiniones = pca.getOpinionesFiltro(filterOptions);
        //modelo.addAttribute("opinionesFiltro", lstOpiniones);
        Gson gson = new Gson();
        List<String> jsonArray = new ArrayList<>();
        for(peop.libs.poolconnection.dao.opinion op :lstOpiniones){
            jsonArray.add(gson.toJson(op));
        }
        
        return jsonArray;
    }

}
