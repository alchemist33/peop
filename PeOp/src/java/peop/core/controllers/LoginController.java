/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.core.controllers;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import peop.infraestructure.PoolConnectionAccess;
import peop.infraestructure.models.usuariosModel;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
@Controller
public class LoginController {

    @InitBinder
    public void miBinder(WebDataBinder binder) {

        StringTrimmerEditor recortaEspacios = new StringTrimmerEditor(true);
        binder.registerCustomEditor(String.class, recortaEspacios);

    }

    @RequestMapping()
    public String muestraLogin() {
        return "Access/Login";
    }

    @RequestMapping("/Index")
    public String muestraIndex() {
        return "Index";
    }

    @RequestMapping("/IndexLogin")
    public String processLogin(@RequestParam("nombreUsuario") String userName,
            @RequestParam("passUsuario") String userPass,
            Model modelo) {

        //String userName = request.getParameter("nombreUsuario");
        //String userPass = request.getParameter("passUsuario");
        if (CheckCredentials(userName, userPass)) {
            return "Index";
        } else {
            return "Access/LoginError";
        }

    }

    @RequestMapping("/Access/peopRegistration")
    public String showregistration(Model model) {

        PoolConnectionAccess pca = new PoolConnectionAccess();

        usuariosModel user = new usuariosModel();
        model.addAttribute("usuario", user);

        List<String> paises = new ArrayList<>();
        List<String> nacionalidades = new ArrayList<>();
        pca.getPaises().forEach(pais -> {
            paises.add(pais.getNombrePais());
            nacionalidades.add(pais.getNacionalidad());
        });

        model.addAttribute("lstPaises", paises);
        model.addAttribute("lstNacionalidades", nacionalidades);

        return "Access/peopRegistration";
    }

    @RequestMapping("/Access/registrarUsuario")
    public String procesarRegistro(@ModelAttribute("usuario") usuariosModel userModel) {

        PoolConnectionAccess pca = new PoolConnectionAccess();
        pca.nuevoUsuario(userModel);
       
        setLogedUser(userModel.getNombre());

        return "Access/postRegistration";

    }

    public boolean CheckCredentials(String userName, String userPass) {

        PoolConnectionAccess pca = new PoolConnectionAccess();
        
        //Guardado del usuario logado. 
       
        boolean accesoConcedido =  pca.checkCredentials(userName, userPass);
        if(accesoConcedido){
             setLogedUser(userName);
        }

        return accesoConcedido;

    }
    
    public void setLogedUser(String userName){
         PoolConnectionAccess pca = new PoolConnectionAccess();
        usuarios us = pca.getUsuario(userName);
        
        
        ProfileController.logedUser.setId(us.getId());
        ProfileController.logedUser.setNombre(us.getNombre());
        ProfileController.logedUser.setUsuarioPeOp(us.getUsuarioPeOp());
        ProfileController.logedUser.setApellidos(us.getApellidos());
        ProfileController.logedUser.setPaisOrigen(us.getPaisOrigen());
        ProfileController.logedUser.setPaisResidencia(us.getPaisResidencia());
        ProfileController.logedUser.setFechaNacimiento(us.getFechaNacimiento());
        ProfileController.logedUser.setSexo(us.getSexo());
    }

}
