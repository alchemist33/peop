/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.core.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import peop.infraestructure.PoolConnectionAccess;
import peop.infraestructure.models.SubcategoriasTemasModel;
import peop.infraestructure.models.options_OpinionGeneralModel;
import peop.infraestructure.models.paisesModel;
import peop.infraestructure.models.temasOpinionModel;
import peop.infraestructure.models.usuariosModel;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
@Controller
public class AdminController {

    @RequestMapping("/Admin/Administrar")
    public String muestraAdminInterna(Model modelo) {

        PoolConnectionAccess pca = new PoolConnectionAccess();
        List<usuarios> lstUsers = pca.getAllUsers();
        List<paisesModel> lstPaises = pca.getPaises();
        List<options_OpinionGeneralModel> lstOptionsOG = pca.getoptions_OpinionGeneral();
         List<temasOpinionModel> lstTemas = pca.getTemasOpinion();

        modelo.addAttribute("AllUsers", lstUsers);
        modelo.addAttribute("AllPaises", lstPaises);
        modelo.addAttribute("options_OpinionGeneral", lstOptionsOG);
         modelo.addAttribute("AllTemas", lstTemas);

        return "Admin/Administrar";
    }

    
    //----------Ajax Area----------------------------------
    @ResponseBody
    @RequestMapping(value = "/Admin/nuevoUsuario_adm", method = RequestMethod.POST)
    public void nuevoUsuario_admin(@RequestBody String request, Model modelo) {

        String[] LstRequest = request.split("&");

        PoolConnectionAccess pca = new PoolConnectionAccess();
        pca.nuevoUsuario(LstRequest);
        List<usuarios> lstUsers = pca.getAllUsers();

        modelo.addAttribute("usuarioCreado", "El usuario se ha creado correctamente");
        modelo.addAttribute("AllUsers", lstUsers);

        //return "2077";
    }

    @ResponseBody
    @RequestMapping(value = "/Admin/borrarUsuario_adm", method = RequestMethod.POST)
    public void borrarUsuario_admin(@RequestBody String request, Model modelo) {

        String[] LstRequest = request.split("=");

        PoolConnectionAccess pca = new PoolConnectionAccess();
        boolean usarioBorrado = pca.borrarUsuario(LstRequest[1]);

        modelo.addAttribute("usuarioBorrado", "El usuario se ha borrado correctamente");

        //return usarioBorrado;
    }

//     @ResponseBody
//    @RequestMapping(value = "/Admin/setIdUsuarioaBorrar", method = RequestMethod.POST)
//    public String setIdUsuarioaBorrar_admin(@RequestBody String request, Model modelo) {
//
//        String[] req = request.split("=");
//        return req[1];
//
//    }
    @ResponseBody
    @RequestMapping(value = "/Admin/nuevoPais_adm", method = RequestMethod.POST)
    public void nuevoPais_admin(@RequestBody String request, Model modelo) {

        String[] LstRequest = request.split("&");

        PoolConnectionAccess pca = new PoolConnectionAccess();
        pca.nuevoPais(LstRequest);
        List<paisesModel> lstPaises = pca.getPaises();

        modelo.addAttribute("usuarioCreado", "El usuario se ha creado correctamente");
        modelo.addAttribute("AllPaises", lstPaises);

        //return "2077";
    }

    @ResponseBody
    @RequestMapping(value = "/Admin/borrarPais_adm", method = RequestMethod.POST)
    public void borrarPais_admin(@RequestBody String request, Model modelo) {

        String[] LstRequest = request.split("=");

        PoolConnectionAccess pca = new PoolConnectionAccess();
        boolean usarioBorrado = pca.borrarPais(LstRequest[1]);

        modelo.addAttribute("usuarioBorrado", "El usuario se ha borrado correctamente");

        //return usarioBorrado;
    }

    @ResponseBody
    @RequestMapping(value = "/Admin/nuevaOpcion_OG_adm", method = RequestMethod.POST)
    public void nuevaOpcion_OG_admin(@RequestBody String request, Model modelo) {

        String[] LstRequest = request.split("&");

        PoolConnectionAccess pca = new PoolConnectionAccess();
        pca.nuevaOpcion_OG(LstRequest);

        modelo.addAttribute("opcionCreada", "La opcion se ha creado correctamente");

        //return "2077";
    }

    @ResponseBody
    @RequestMapping(value = "/Admin/borrarOpcion_OG_adm", method = RequestMethod.POST)
    public void borrarOpcion_OG_admin(@RequestBody String request, Model modelo) {

        String[] LstRequest = request.split("=");

        PoolConnectionAccess pca = new PoolConnectionAccess();
        boolean opcionBorrada = pca.borrarOpcion_OG(LstRequest[1]);

        modelo.addAttribute("opcionBorrada", "La opcion se ha borrado correctamente");

        //return usarioBorrado;
    }

}
