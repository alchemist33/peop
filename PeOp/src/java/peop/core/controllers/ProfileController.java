/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.core.controllers;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import peop.infraestructure.PoolConnectionAccess;
import peop.infraestructure.models.opinionModel;
import peop.infraestructure.models.usuariosModel;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
@Controller
public class ProfileController {

    public static usuarios logedUser = new usuarios();

    @RequestMapping("/Profile/verPerfil")
    public String muestraPerfil(@ModelAttribute("usuario") usuariosModel usModel, Model modelo) {

        usuariosModel user = new usuariosModel();
        user.setId(logedUser.getId());
        user.setNombre(logedUser.getNombre());
        user.setUsuarioPeOp(logedUser.getUsuarioPeOp());
        user.setApellidos(logedUser.getApellidos());
        user.setPaisOrigen(logedUser.getPaisOrigen());
        user.setPaisResidencia(logedUser.getPaisResidencia());
        user.setFechaNacimiento(logedUser.getFechaNacimiento());
        user.setSexo(logedUser.getSexo());

        modelo.addAttribute("Usuario", user);

        return "/Profile/verPerfil";
    }

    @RequestMapping("/Profile/HistoricoOpinion")
    public String muestraHistoricoOpinion(Model modelo) {

        PoolConnectionAccess pca = new PoolConnectionAccess();
        List<opinionModel> lstOps = pca.getOpinionesUsuario(logedUser.getId());

        modelo.addAttribute("OpinionesUsuario", lstOps);
        return "/Profile/HistoricoOpinion";
    }

    @RequestMapping("/Profile/Recomendar")
    public String muestraRecomendar() {
        return "/Profile/Recomendar";
    }

    @RequestMapping("/Profile/actualizarPerfil")
    public String actualizarPerfil(@ModelAttribute("usuario") usuariosModel usModel, Model modelo) {

        PoolConnectionAccess pca = new PoolConnectionAccess();

        pca.editaDatosUsuario(usModel);
        setLogedUser(usModel.getId());

        modelo.addAttribute("Usuario", logedUser);
        return "/Profile/verPerfil";
    }

    public void setLogedUser(String IdUsuario) {
        PoolConnectionAccess pca = new PoolConnectionAccess();
        usuarios us = pca.getUsuarioById(IdUsuario);

        logedUser.setId(us.getId());
        logedUser.setNombre(us.getNombre());
        logedUser.setUsuarioPeOp(us.getUsuarioPeOp());
        logedUser.setApellidos(us.getApellidos());
        logedUser.setPaisOrigen(us.getPaisOrigen());
        logedUser.setPaisResidencia(us.getPaisResidencia());
        logedUser.setFechaNacimiento(us.getFechaNacimiento());
        logedUser.setSexo(us.getSexo());
    }

}
