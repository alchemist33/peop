/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.core.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author alquimista
 */
public class OpinionValidacion implements ConstraintValidator<OpinionValidation, String> {

    private String valoracion;

    @Override
    public void initialize(OpinionValidation a) {
        valoracion = a.value();
        
    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        boolean valCodigo;
        if (t != null) {
            valCodigo = t.startsWith(valoracion);
        } else {
            valCodigo = true;
            return valCodigo;
        }

        return valCodigo;

    }

}
