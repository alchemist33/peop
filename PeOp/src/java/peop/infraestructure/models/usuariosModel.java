/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure.models;

import java.sql.Date;
import java.util.UUID;

/**
 *
 * @author alquimista
 */
public class usuariosModel {

    private String Id;
    private String nombre;
    private String apellidos;
    private Date fechaNacimiento;
    private String nacionalidad;
    private String paisResidencia;
    private String paisOrigen;
    private String sexo;
    private String usuarioPeOp;
    private String userPass;
    private Date fechaAlta;
    private String email;

    public usuariosModel() {
    }

    public usuariosModel(String Id, String nombre, String apellidos, Date fechaNacimiento, String nacionalidad, String paisResidencia, String paisOrigen, String sexo, String usuarioPeOp, String userPass, Date fechaAlta, String email) {
        this.Id = Id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.nacionalidad = nacionalidad;
        this.paisResidencia = paisResidencia;
        this.paisOrigen = paisOrigen;
        this.sexo = sexo;
        this.usuarioPeOp = usuarioPeOp;
        this.userPass = userPass;
        this.fechaAlta = fechaAlta;
        this.email = email;
    }



    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getUsuarioPeOp() {
        return usuarioPeOp;
    }

    public void setUsuarioPeOp(String usuarioPeOp) {
        this.usuarioPeOp = usuarioPeOp;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
