/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure.models;

/**
 *
 * @author alquimista
 */
public class categoriasTemasModel {

    private String IdCategoria;
    private String NombreCategoria;
    private String NombreSubCategoria;

    public categoriasTemasModel() {
    }

    public categoriasTemasModel(String IdCategoria, String NombreCategoria, String NombreSubCategoria) {
        this.IdCategoria = IdCategoria;
        this.NombreCategoria = NombreCategoria;
        this.NombreSubCategoria = NombreSubCategoria;
    }

   

    public String getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(String IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    public String getNombreCategoria() {
        return NombreCategoria;
    }

    public void setNombreCategoria(String NombreCategoria) {
        this.NombreCategoria = NombreCategoria;
    }

    public String getNombreSubCategoria() {
        return NombreSubCategoria;
    }

    public void setNombreSubCategoria(String NombreSubCategoria) {
        this.NombreSubCategoria = NombreSubCategoria;
    }

}
