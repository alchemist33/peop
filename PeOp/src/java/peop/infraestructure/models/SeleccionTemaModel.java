/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure.models;

/**
 *
 * @author alquimista
 */
public class SeleccionTemaModel {

    private String nombreCategoria;
    private String nombreSubCategoria;
    private String nombreTema;
    private String IdCategoria;
    private String IdSubCategoria;
    private String IdTema;

    public SeleccionTemaModel() {
    }

    public SeleccionTemaModel(String nombreCategoria, String nombreSubCategoria, String nombreTema, String IdCategoria, String IdSubCategoria, String IdTema) {
        this.nombreCategoria = nombreCategoria;
        this.nombreSubCategoria = nombreSubCategoria;
        this.nombreTema = nombreTema;
        this.IdCategoria = IdCategoria;
        this.IdSubCategoria = IdSubCategoria;
        this.IdTema = IdTema;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getNombreSubCategoria() {
        return nombreSubCategoria;
    }

    public void setNombreSubCategoria(String nombreSubCategoria) {
        this.nombreSubCategoria = nombreSubCategoria;
    }

    public String getNombreTema() {
        return nombreTema;
    }

    public void setNombreTema(String nombreTema) {
        this.nombreTema = nombreTema;
    }

    public String getIdCategoria() {
        return IdCategoria;
    }

    public void setIdCategoria(String IdCategoria) {
        this.IdCategoria = IdCategoria;
    }

    public String getIdSubCategoria() {
        return IdSubCategoria;
    }

    public void setIdSubCategoria(String IdSubCategoria) {
        this.IdSubCategoria = IdSubCategoria;
    }

    public String getIdTema() {
        return IdTema;
    }

    public void setIdTema(String IdTema) {
        this.IdTema = IdTema;
    }

}
