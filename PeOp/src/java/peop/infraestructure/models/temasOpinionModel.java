/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure.models;

import java.util.UUID;

/**
 *
 * @author alquimista
 */
public class temasOpinionModel {

    public String IdTema;
    public String IdCategoriaTema;
    public String IdSubCategoriaTema;
    public String nombreTema;

    public temasOpinionModel() {
    }

    public temasOpinionModel(String IdTema, String IdCategoriaTema, String IdSubCategoriaTema, String nombreTema) {
        this.IdTema = IdTema;
        this.IdCategoriaTema = IdCategoriaTema;
        this.IdSubCategoriaTema = IdSubCategoriaTema;
        this.nombreTema = nombreTema;
    }

    public String getIdTema() {
        return IdTema;
    }

    public void setIdTema(String IdTema) {
        this.IdTema = IdTema;
    }

    public String getIdCategoriaTema() {
        return IdCategoriaTema;
    }

    public void setIdCategoriaTema(String IdCategoriaTema) {
        this.IdCategoriaTema = IdCategoriaTema;
    }

    public String getIdSubCategoriaTema() {
        return IdSubCategoriaTema;
    }

    public void setIdSubCategoriaTema(String IdSubCategoriaTema) {
        this.IdSubCategoriaTema = IdSubCategoriaTema;
    }

    public String getNombreTema() {
        return nombreTema;
    }

    public void setNombreTema(String nombreTema) {
        this.nombreTema = nombreTema;
    }

}
