/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure.models;

/**
 *
 * @author alquimista
 */
public class paisesModel {

    private Integer IdPais;
    private String nombrePais;
    private String nacionalidad;

    public paisesModel() {
    }

    public paisesModel(Integer IdPais, String nombrePais, String nacionalidad) {
        this.IdPais = IdPais;
        this.nombrePais = nombrePais;
        this.nacionalidad = nacionalidad;
    }

    public Integer getIdPais() {
        return IdPais;
    }

    public void setIdPais(Integer IdPais) {
        this.IdPais = IdPais;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

}
