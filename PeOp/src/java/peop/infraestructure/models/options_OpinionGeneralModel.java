/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure.models;

/**
 *
 * @author alquimista
 */
public class options_OpinionGeneralModel {

    private int idOption_OpinionGeneral;
    private String opcion;
    private int orden;

    public options_OpinionGeneralModel() {
    }

    public options_OpinionGeneralModel(int idOption_OpinionGeneral, String opcion, int orden) {
        this.idOption_OpinionGeneral = idOption_OpinionGeneral;
        this.opcion = opcion;
        this.orden = orden;
    }

    public int getIdOption_OpinionGeneral() {
        return idOption_OpinionGeneral;
    }

    public void setIdOption_OpinionGeneral(int idOption_OpinionGeneral) {
        this.idOption_OpinionGeneral = idOption_OpinionGeneral;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

}
