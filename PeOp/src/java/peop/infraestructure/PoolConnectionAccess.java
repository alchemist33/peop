/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import peop.core.EncryptorTool;
import peop.infraestructure.models.SubcategoriasTemasModel;
import peop.infraestructure.models.categoriasTemasModel;
import peop.infraestructure.models.opinionModel;
import peop.infraestructure.models.options_OpinionGeneralModel;
import peop.infraestructure.models.paisesModel;
import peop.infraestructure.models.temasOpinionModel;
import peop.infraestructure.models.usuariosModel;
import peop.libs.poolconnection.PoolConnectionCore;
import peop.libs.poolconnection.PoolConnectionCore.tipoConex;
import peop.libs.poolconnection.dao.TemasOpinion;
import peop.libs.poolconnection.dao.opinion;

import peop.libs.poolconnection.dao.paises;
import peop.libs.poolconnection.dao.paises2;
import peop.libs.poolconnection.dao.usuarios;
import peop.libs.poolconnection.datakit.ClientAccessData;

/**
 *
 * @author alquimista
 */
public class PoolConnectionAccess {

    PoolConnectionCore pcc = new PoolConnectionCore();
    GetData gd = new GetData();

    ClientAccessData cad = new ClientAccessData();

    public enum methods {
        getPaises;
    }

    public Connection getConex(tipoConex type) {

        Connection con = pcc.getConex(type);
        return con;
    }

    public List<usuarios> getAllUsers() {
        Connection con = getConex(PoolConnectionCore.tipoConex.SQLite);
        return gd.getAllUsers(con);
    }

    public void nuevoUsuario(usuariosModel userModel) {
        TypeMappers tym = new TypeMappers();
        cad.nuevoUsuario(tym.usuariosModel_MAPTO_usuarios(userModel, false));
    }

    public void nuevoUsuario(String[] request) {
        AjaxMappers ajm = new AjaxMappers();
        cad.nuevoUsuario(ajm.AjaxObject_MAPTO_usuarios(request, false));
    }

    public List<categoriasTemasModel> getCategorias() {
        TypeMappers tym = new TypeMappers();
        List<categoriasTemasModel> lst = new ArrayList<>();

        lst = tym.categoriasTemas_MAPTO_categoriasTemasModel(cad.getCategoriasTemas());
        return lst;
    }

    public List<SubcategoriasTemasModel> getSubCategorias() {
        TypeMappers tym = new TypeMappers();
        List<SubcategoriasTemasModel> lst = new ArrayList<>();
        lst = tym.SubCategoriasTemas_MAPTO_SubCategoriasTemasModel(cad.getSubCategoriasTemas());
        return lst;
    }

    public List<temasOpinionModel> getTemasOpinion() {
        TypeMappers tym = new TypeMappers();
        List<temasOpinionModel> lst = new ArrayList<>();
        lst = tym.TemasOpinion_MAPTO_temasOpinionModel(cad.getTemasOpinion());
        return lst;
    }

    public List<options_OpinionGeneralModel> getoptions_OpinionGeneral() {
        TypeMappers tym = new TypeMappers();
        List<options_OpinionGeneralModel> lst = new ArrayList<>();
        lst = tym.options_OpinionGeneral_MAPTO_options_OpinionGeneralModel(cad.getOptions_OpinionGeneral());
        return lst;
    }

    public void nuevaOpinion(opinionModel opm) {

        TypeMappers tym = new TypeMappers();
        opinion op = tym.opinionModel_MAPTO_opinion(opm);
        cad.nuevaOpinion(op);
    }

    public List<paisesModel> getPaises() {
        TypeMappers tym = new TypeMappers();
        List<paisesModel> lst = new ArrayList<>();
        lst = tym.paises_MAPTO_paisesModel(cad.getPaises());
        return lst;
    }

    public boolean checkCredentials(String userName, String userPass) {
         String encryptPass = "";
        try {
            encryptPass = EncryptorTool.encrypt(userPass);
        } catch (Exception ex) {
            Logger.getLogger(PoolConnectionAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cad.checkCredentials(userName, encryptPass);
    }

    public String getIdUsuario(String userName) {
        return cad.getIdusuario(userName);
    }

    public usuarios getUsuario(String userName) {
        return cad.getUsuario(userName);
    }

    public usuarios getUsuarioById(String IdUsuario) {
        return cad.getUsuarioById(IdUsuario);
    }

    public boolean editaDatosUsuario(usuariosModel userModel) {
        TypeMappers tym = new TypeMappers();
        return cad.editaDatosUsuarios(tym.usuariosModel_MAPTO_usuarios(userModel, true));
    }

    public boolean borrarUsuario(String IdUsuario) {
        return cad.borrarUsuario(IdUsuario);
    }

    public void nuevoPais(String[] paisModel) {
        AjaxMappers ajm = new AjaxMappers();
        cad.nuevoPais(ajm.AjaxObject_paisModel_MAPTO_pais(paisModel, false));
    }
    
    public boolean borrarPais(String IdPais) {
        return cad.borrarPais(IdPais);
    }
    
    public boolean borrarOpcion_OG(String IdOpcion) {
        return cad.borrarOpcion_OG(IdOpcion);
    }
    
    public void nuevaOpcion_OG(String[] options_OGModel) {
        AjaxMappers ajm = new AjaxMappers();
        cad.nuevaOpcion_OG(ajm.AjaxObject_options_OGModel_MAPTO_options_OG(options_OGModel, false));
    }
    
    public List<opinionModel> getOpinionesUsuario(String IdUsuario) {
        TypeMappers tym = new TypeMappers();
        return  tym.opinion_MAPTO_opinionModel(cad.getOpinionesUsuario(IdUsuario));
    }
    
    public List<opinion> getOpinionesFiltro(String[] filterOptions) {
        AjaxMappers ajm = new AjaxMappers();
        usuarios us = ajm.AjaxObject_filterOptionsModel_MAPTO_filterOptions(filterOptions);
        
        String pR = us.getPaisResidencia();
        String pO = us.getPaisOrigen();
        String fN = us.getFechaNacimiento().toString();
        String sX = us.getSexo();
        
        List<opinion> lst = new ArrayList<>();
        lst = cad.getOpinionesFiltro(pR,pO,fN,sX);
        return  lst;
    }
}
