/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure;

import java.sql.Date;
import peop.infraestructure.models.paisesModel;
import peop.libs.poolconnection.dao.opinion;
import peop.libs.poolconnection.dao.options_OpinionGeneral;
import peop.libs.poolconnection.dao.paises2;
import peop.libs.poolconnection.dao.userPass;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
public class AjaxMappers {

    public usuarios AjaxObject_MAPTO_usuarios(String[] request, boolean update) {

        usuarios user = new usuarios();
        user.setNombre(getValue(request[0]));//Nombre
        user.setApellidos(getValue(request[1]));//Apellidos
        user.setFechaNacimiento(Date.valueOf(getValue(request[2])));//fechaNacimiento
        user.setEmail(getValue(request[3]));//email
        user.setUsuarioPeOp(getValue(request[4]));//usuarioPeOp
        user.setUserPass(getValue(request[5]));//pass

        /*
        user.setPaisOrigen(getValue(request[3]));//paisOrigen
        user.setPaisResidencia(getValue(request[4]));//paisResidencia
        user.setSexo(getValue(request[5]));//Sexo
       

        if (update) {
            user.setId(getValue(request[8]));//Id Usuario
            user.setFechaAlta(Date.valueOf(getValue(request[9])));//fecha Alta
        }
         */
        return user;

    }

    public paises2 AjaxObject_paisModel_MAPTO_pais(String[] request, boolean update) {

        paises2 p = new paises2();

        p.setNombrePais(getValue(request[0]));
        p.setNacionalidad(getValue(request[1]));

        return p;
    }

    public options_OpinionGeneral AjaxObject_options_OGModel_MAPTO_options_OG(String[] request, boolean update) {

        options_OpinionGeneral op_og = new options_OpinionGeneral();

        op_og.setOpcion(getValue(request[0]));
        op_og.setOrden(Integer.valueOf(getValue(request[1])));

        return op_og;
    }

    
    
    public usuarios AjaxObject_filterOptionsModel_MAPTO_filterOptions(String[] filterOptions) {
    
        usuarios us = new usuarios();
        
        us.setPaisResidencia(getValue(filterOptions[0]));
        us.setPaisOrigen(getValue(filterOptions[1]));
        us.setFechaNacimiento(Date.valueOf(getValue(filterOptions[2])));
        us.setSexo(getValue(filterOptions[3]));
        return us;
        
    }

    
    
    public String getValue(String keyValue) {
        String[] kv = keyValue.split("=");
        return kv[1];
    }
}
