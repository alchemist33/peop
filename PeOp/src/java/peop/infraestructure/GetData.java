/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import peop.libs.poolconnection.dao.*;
import peop.libs.poolconnection.PoolConnectionCore;

/**
 *
 * @author alquimista
 */
public class GetData {



    public List<usuarios> getAllUsers(Connection con) {

        try {
            List<usuarios> lstUsers;
            lstUsers = new ArrayList<>();

            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM usuarios");

            while (rs.next()) {
                usuarios user = new usuarios();
                //InputStream input = rs.getBinaryStream("Id");
                user.setId(rs.getString("Id"));
                user.setNombre(rs.getString("nombre"));
                user.setApellidos(rs.getString("apellidos"));
                user.setFechaNacimiento(rs.getDate("fechaNacimiento"));
                user.setNacionalidad(rs.getString("nacionalidad"));
                user.setPaisResidencia(rs.getString("paisResidencia"));
                user.setPaisOrigen(rs.getString("paisOrigen"));
                user.setSexo(rs.getString("sexo"));
                user.setUsuarioPeOp(rs.getString("usuarioPeoP"));

                lstUsers.add(user);
            }
            con.close();
            return lstUsers;
        } catch (SQLException ex) {
            Logger.getLogger(PoolConnectionAccess.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(GetData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    

    

}
