/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.infraestructure;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import peop.core.EncryptorTool;
import peop.infraestructure.models.SubcategoriasTemasModel;
import peop.infraestructure.models.categoriasTemasModel;
import peop.infraestructure.models.opinionModel;
import peop.infraestructure.models.options_OpinionGeneralModel;
import peop.infraestructure.models.paisesModel;
import peop.infraestructure.models.temasOpinionModel;
import peop.infraestructure.models.usuariosModel;
import peop.libs.poolconnection.dao.*;
import peop.libs.poolconnection.dao.usuarios;

/**
 *
 * @author alquimista
 */
public class TypeMappers {

    public <T> T Mapper(Class<T> type1, Class<T> type2) {

        for (Field prop : type1.getDeclaredFields()) {

        }

        return (T) type1;
    }

    //Si no es una actualizacion de datos el constructor de usuarios de la librearia crea automaticamente los valores de Id y fechaAlta
    public usuarios usuariosModel_MAPTO_usuarios(usuariosModel userModel, boolean update) {

        usuarios user = new usuarios();

        user.setNombre(userModel.getNombre());
        user.setApellidos(userModel.getApellidos());
        user.setFechaNacimiento(userModel.getFechaNacimiento());
        user.setNacionalidad(userModel.getNacionalidad());
        user.setPaisResidencia(userModel.getPaisResidencia());
        user.setPaisOrigen(userModel.getPaisOrigen());
        user.setSexo(userModel.getSexo());
        user.setUsuarioPeOp(userModel.getUsuarioPeOp());

      
      

        try {
            String encryptPass = EncryptorTool.encrypt(userModel.getUserPass());
            //String decryptPass = EncryptorTool.decrypt(EncryptorTool.encrypt(userModel.getUserPass()));
            user.setUserPass(encryptPass);
        } catch (Exception ex) {
            Logger.getLogger(TypeMappers.class.getName()).log(Level.SEVERE, null, ex);
        }

        

        user.setEmail(userModel.getEmail());

        if (update) {
            user.setId(userModel.getId());
            user.setFechaAlta(userModel.getFechaAlta());
        }

        return user;
    }

    public usuariosModel usuarios_MAPTO_usuariosModel(usuarios user) {

        usuariosModel userModel = new usuariosModel();

        return userModel;
    }

    public List<categoriasTemasModel> categoriasTemas_MAPTO_categoriasTemasModel(List<CategoriasTemas> lstCatsTemas) {

        List<categoriasTemasModel> lstCatsTemasModel = new ArrayList<>();

        lstCatsTemas.stream().map(cat -> {

            categoriasTemasModel ctm = new categoriasTemasModel();
            ctm.setIdCategoria(cat.getIdCategoria());
            ctm.setNombreCategoria(cat.getNombreCategoria());
            return ctm;
        }).forEachOrdered(ctm -> {
            lstCatsTemasModel.add(ctm);
        });

        return lstCatsTemasModel;
    }

    public List<SubcategoriasTemasModel> SubCategoriasTemas_MAPTO_SubCategoriasTemasModel(List<SubCategoriasTemas> lstSubCatsTemas) {

        List<SubcategoriasTemasModel> lstSubCatsTemasModel = new ArrayList<>();

        lstSubCatsTemas.stream().map(subCat -> {
            SubcategoriasTemasModel sctm = new SubcategoriasTemasModel();
            sctm.setIdSubCategoria(subCat.getIdSubCategoria());
            sctm.setIdCategoria(subCat.getIdCategoria());
            sctm.setNombreSubCategoria(subCat.getNombreSubCategoria());
            return sctm;
        }).forEachOrdered(sctm -> {
            lstSubCatsTemasModel.add(sctm);
        });

        return lstSubCatsTemasModel;
    }

    public List<temasOpinionModel> TemasOpinion_MAPTO_temasOpinionModel(List<TemasOpinion> lstTemasOpinion) {

        List<temasOpinionModel> lstTemasOpinionModel = new ArrayList<>();

        lstTemasOpinion.stream().map(tema -> {
            temasOpinionModel tom = new temasOpinionModel();
            tom.setIdTema(tema.getIdTema());
            tom.setIdCategoriaTema(tema.getIdCategoriaTema());
            tom.setIdSubCategoriaTema(tema.getIdSubCategoriaTema());
            tom.setNombreTema(tema.getNombreTema());
            return tom;
        }).forEachOrdered(tom -> {
            lstTemasOpinionModel.add(tom);
        });

        return lstTemasOpinionModel;
    }

    public List<options_OpinionGeneralModel> options_OpinionGeneral_MAPTO_options_OpinionGeneralModel(List<options_OpinionGeneral> lstTOptions_OpinionGeneral) {

        List<options_OpinionGeneralModel> lstOptions_OpinionGeneralModel = new ArrayList<>();

        lstTOptions_OpinionGeneral.stream().map(opt -> {
            options_OpinionGeneralModel opt_ogm = new options_OpinionGeneralModel();
            opt_ogm.setIdOption_OpinionGeneral(opt.getIdOption_OpinionGeneral());
            opt_ogm.setOpcion(opt.getOpcion());
            opt_ogm.setOrden(opt.getOrden());
            return opt_ogm;
        }).forEachOrdered(opt_ogm -> {
            lstOptions_OpinionGeneralModel.add(opt_ogm);
        });

        return lstOptions_OpinionGeneralModel;
    }

    public opinion opinionModel_MAPTO_opinion(opinionModel opm) {

        return new opinion(
                opm.getTema(),
                opm.getValoracion(),
                opm.getOpinionGeneral(),
                opm.getVom(),
                opm.getIdCategoria(),
                opm.getIdSubCategoria(),
                opm.getIdTema(),
                opm.getIdUsuario());
    }

    public List<opinionModel> opinion_MAPTO_opinionModel(List<opinion> lstOps) {
        List<opinionModel> lstOpsModel = new ArrayList<>();
        lstOps.stream().map(op -> {
            opinionModel opm = new opinionModel();
            opm.setIdOpinion(op.getIdOpinion());
            opm.setIdCategoria(op.getIdCategoria());
            opm.setIdSubCategoria(op.getIdSubCategoria());
            opm.setIdTema(op.getIdTema());
            opm.setIdUsuario(op.getIdUsuario());
            opm.setTema(op.getTema());
            opm.setOpinionGeneral(op.getOpinionGeneral());
            opm.setValoracion(op.getValoracion());
            opm.setVom(op.getVom());
            return opm;
        }).forEachOrdered(opm -> {
            lstOpsModel.add(opm);
        });

        return lstOpsModel;
    }

    public List<paisesModel> paises_MAPTO_paisesModel(List<paises2> lstPaises) {
        List<paisesModel> lstPaisesModel = new ArrayList<>();

        lstPaises.stream().map(pais -> {
            paisesModel p = new paisesModel();
            p.setIdPais(pais.getIdPais());
            p.setNombrePais(pais.getNombrePais());
            p.setNacionalidad(pais.getNacionalidad());
            return p;
        }).forEachOrdered(p -> {
            lstPaisesModel.add(p);
        });

        return lstPaisesModel;

    }

}
