BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "user_sequence" (
	"next_val"	INTEGER UNIQUE,
	PRIMARY KEY("next_val" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "paises" (
	"IdPais"	TEXT UNIQUE,
	"nombrePais"	TEXT,
	"nacionalidad"	TEXT,
	PRIMARY KEY("IdPais")
);
CREATE TABLE IF NOT EXISTS "usuarios" (
	"Id"	BLOB UNIQUE,
	"nombre"	TEXT,
	"apellidos"	TEXT,
	"fechaNacimiento"	TEXT,
	"nacionalidad"	TEXT,
	"paisOrigen"	TEXT,
	"paisResidencia"	TEXT,
	"sexo"	TEXT,
	"usuarioPeOp"	TEXT,
	PRIMARY KEY("Id")
);
CREATE TABLE IF NOT EXISTS "CategoriasTemas" (
	"IdCategoria"	TEXT NOT NULL UNIQUE,
	"NombreCategoria"	TEXT,
	PRIMARY KEY("IdCategoria")
);
CREATE TABLE IF NOT EXISTS "SubCategoriasTemas" (
	"IdSubCategoria"	TEXT UNIQUE,
	"IdCategoriaTema"	TEXT NOT NULL,
	"NombreSubCategoria"	TEXT NOT NULL,
	PRIMARY KEY("IdSubCategoria")
);
CREATE TABLE IF NOT EXISTS "TemasOpinion" (
	"IdTema"	BLOB UNIQUE,
	"IdCategoriaTema"	TEXT,
	"IdSubCategoriaTema"	TEXT,
	"NombreTema"	TEXT,
	PRIMARY KEY("IdTema")
);
CREATE TABLE IF NOT EXISTS "OpinionesUsuarios" (
	"IdOpinion"	TEXT UNIQUE,
	"IdCategoriaTema"	TEXT,
	"IdSubcategoriaTema"	TEXT,
	"IdTema"	TEXT,
	"ValoracionTema"	NUMERIC,
	"opinionGeneral"	TEXT,
	PRIMARY KEY("IdOpinion")
);
CREATE TABLE IF NOT EXISTS "options_OpinionGeneral" (
	"IdOption_OpinionGeneral"	INTEGER NOT NULL UNIQUE,
	"Opcion"	TEXT,
	"orden"	INTEGER,
	PRIMARY KEY("IdOption_OpinionGeneral" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "test" (
	"Field1"	INTEGER NOT NULL,
	PRIMARY KEY("Field1" AUTOINCREMENT)
);
INSERT INTO "user_sequence" ("next_val") VALUES (0);
INSERT INTO "paises" ("IdPais","nombrePais","nacionalidad") VALUES ('1','España','Española');
INSERT INTO "paises" ("IdPais","nombrePais","nacionalidad") VALUES ('2','Italia','Italiana');
INSERT INTO "usuarios" ("Id","nombre","apellidos","fechaNacimiento","nacionalidad","paisOrigen","paisResidencia","sexo","usuarioPeOp") VALUES ('��f�:OT��q�����','Alex','Helios,','1610406000000','EspaÃ±ola','EspaÃ±a','España','Hombre','us1');
INSERT INTO "usuarios" ("Id","nombre","apellidos","fechaNacimiento","nacionalidad","paisOrigen","paisResidencia","sexo","usuarioPeOp") VALUES ('DD��&L��}���ڌ','El pony','pisador','1609801200000','Italiana','EspaÃ±a','España','Alternativo','us3');
INSERT INTO "usuarios" ("Id","nombre","apellidos","fechaNacimiento","nacionalidad","paisOrigen","paisResidencia","sexo","usuarioPeOp") VALUES ('Z��=C�����*3[','Mickey','Mouse','1609369200000','Italiana','Italia','España','Alternativo','us4');
INSERT INTO "usuarios" ("Id","nombre","apellidos","fechaNacimiento","nacionalidad","paisOrigen","paisResidencia","sexo","usuarioPeOp") VALUES ('*���LH����m�','Mickey','Mouse','1609714800000','Italiana','Italia','España','Hombre','us5');
INSERT INTO "usuarios" ("Id","nombre","apellidos","fechaNacimiento","nacionalidad","paisOrigen","paisResidencia","sexo","usuarioPeOp") VALUES ('Hk:N��Hy�dWR��:i','Julia','Memba','1609714800000','Italiana','Italia','España','Mujer','us6');
INSERT INTO "usuarios" ("Id","nombre","apellidos","fechaNacimiento","nacionalidad","paisOrigen","paisResidencia","sexo","usuarioPeOp") VALUES ('null','Marco2','Memba','2020-12-29','EspaÃ±ola','EspaÃ±a','España','Hombre','us7');
INSERT INTO "CategoriasTemas" ("IdCategoria","NombreCategoria") VALUES ('8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','Arte');
INSERT INTO "CategoriasTemas" ("IdCategoria","NombreCategoria") VALUES ('3168F1FD-B928-4E55-A623-61F60CD9D063','Deportes');
INSERT INTO "CategoriasTemas" ("IdCategoria","NombreCategoria") VALUES ('721FC09A-143C-4DB2-8096-A781D13C7546','Cultura y Sociedad');
INSERT INTO "CategoriasTemas" ("IdCategoria","NombreCategoria") VALUES ('5092761F-74A2-47CF-89C8-DE121ABDD287','Ciencias');
INSERT INTO "CategoriasTemas" ("IdCategoria","NombreCategoria") VALUES ('26F32F55-8F16-42DB-8447-4C37D34ABBB9','Eventos Historicos');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('C41B8A9F-FCFD-4C77-B8D4-7A6F0F5ACBCE','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','Pintura');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('2928F508-D6C4-4DE7-832D-1801D3B340F2','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','Escultura');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('EA5074AA-398F-4DB3-A225-57D552D37067','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','Cine');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('D7539A92-017F-4755-AE4B-BF354707C6A4','3168F1FD-B928-4E55-A623-61F60CD9D063','Baloncesto');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('B7368B9B-AF57-43B1-A2B2-81E3AFA49FA8','3168F1FD-B928-4E55-A623-61F60CD9D063','Futbol');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('EF67CA5B-0BFF-42B0-A7A8-E09925B7354A','26F32F55-8F16-42DB-8447-4C37D34ABBB9','Guerras');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('CA1C71DA-0FC8-4D5E-8803-E052EB28EFBE','26F32F55-8F16-42DB-8447-4C37D34ABBB9','Hitos Cientificos');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('25456254-291C-4378-95A4-87D7C34FA89C','5092761F-74A2-47CF-89C8-DE121ABDD287','Física');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('3B445A96-5ACF-4869-971A-803EE3F23507','5092761F-74A2-47CF-89C8-DE121ABDD287','Ingeniería');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('48C03DB8-E69F-4BFB-9E22-A120A8591B80','721FC09A-143C-4DB2-8096-A781D13C7546','Costumbres');
INSERT INTO "SubCategoriasTemas" ("IdSubCategoria","IdCategoriaTema","NombreSubCategoria") VALUES ('AF071170-BDA2-4DE1-A360-164070A42BA1','721FC09A-143C-4DB2-8096-A781D13C7546','Sabiduría popular');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('5c8db775-e40f-4d9a-843b-d8ed18ee8120','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','C41B8A9F-FCFD-4C77-B8D4-7A6F0F5ACBCE','El jardin de las delicias');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('e1de10a9-8822-44a4-acf6-f313748ac6a0','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','C41B8A9F-FCFD-4C77-B8D4-7A6F0F5ACBCE','El grito');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('1c288f35-aaf4-4141-9a8b-eaf09d280b0f','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','EA5074AA-398F-4DB3-A225-57D552D37067','El gran dictador');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('00f5a971-84cd-48c9-8ed7-b8f3ff6bc2ca','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','EA5074AA-398F-4DB3-A225-57D552D37067','Malditos bastardos');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('9cea05f8-b7a6-4dc3-a850-1fa08b97ea3f','8CDC5EEC-BA38-4F8B-BDD2-EF83EA72039C','2928F508-D6C4-4DE7-832D-1801D3B340F2','El David');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('5fc683b8-aecb-455d-add5-639b8a6f917a','3168F1FD-B928-4E55-A623-61F60CD9D063','D7539A92-017F-4755-AE4B-BF354707C6A4','Baloncesto Americano');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('86E7F79F-D709-473C-8459-2B3023DF0002','3168F1FD-B928-4E55-A623-61F60CD9D063','D7539A92-017F-4755-AE4B-BF354707C6A4','Baloncesto Europeo');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('0FC2189E-8AC7-4104-9AC0-AC014403E7D6','26F32F55-8F16-42DB-8447-4C37D34ABBB9','CA1C71DA-0FC8-4D5E-8803-E052EB28EFBE','LLegada a la luna del primer ser humano');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('ED0E8665-6503-4C24-B064-4560CCF29C71','26F32F55-8F16-42DB-8447-4C37D34ABBB9','EF67CA5B-0BFF-42B0-A7A8-E09925B7354A','Segunda Guerra Mundial');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('8F4C819E-AA26-4E2A-8E59-FB8C61D8A636','5092761F-74A2-47CF-89C8-DE121ABDD287','25456254-291C-4378-95A4-87D7C34FA89C','Astrofísica');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('C1D9FE6B-CB7E-4957-999B-5121D2BC3767','5092761F-74A2-47CF-89C8-DE121ABDD287','25456254-291C-4378-95A4-87D7C34FA89C','Fisica Cuantica');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('64FFF175-06F0-408E-8FCF-FF595A8BAFEF','721FC09A-143C-4DB2-8096-A781D13C7546','AF071170-BDA2-4DE1-A360-164070A42BA1','Refranero Español');
INSERT INTO "TemasOpinion" ("IdTema","IdCategoriaTema","IdSubCategoriaTema","NombreTema") VALUES ('ED395988-8258-4D1F-8CCC-2E3975EC1080','721FC09A-143C-4DB2-8096-A781D13C7546','48C03DB8-E69F-4BFB-9E22-A120A8591B80','12 uvas con las campanadas de fin de año');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('��%�JGۦx�V�kLI',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�(�5C��S�h5yߜ',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�6�&NvO���Z�c��',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('k�D�PaM���P�e�g',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('��sd��H���t�����',NULL,NULL,NULL,NULL,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�R�d�LC��P��U�d�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('?��A�bC	�и���1',NULL,NULL,NULL,NULL,NULL);
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�YZqfA���I\���',NULL,NULL,NULL,NULL,NULL);
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('ar�M�K���T�[��',NULL,NULL,NULL,NULL,NULL);
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�.2��fHf��A�4y�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('e��[VE��WJ@�.�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('ӥe��I=�O���RP�',NULL,NULL,NULL,50,NULL);
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���F\?B؛[Yc^��y',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('/s��K�G��j��*�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('E�>W|�M��x�h�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('2b�l�H2�祉�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�aM8WUC����c����',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('~�eգ�E�����rV�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���}F���.�p',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('Xh^@��O����CCT�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES (X'6fa14e3751d948bd9a62ee7800341148',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('5�����L����^`��',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���|J~��5j#QZ�',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('��;	�P@��E�x���',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('M@�K|�KK���',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('_�YF�@''�M�K�VV',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���P�MOr�LPW���',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���(|�Bj�~!��I��',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('#F�n�+A��mY+���Y',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('X-��O��~�:�U��',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES (X'e6c600d09692404a867ac68409e569f1',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES (X'2382dc1ec52b4dfd99136593a5e95000',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���5?�M��tŔ���',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('������B�}]�,O>�',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�B���DE�����#�',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�����OS�մ�)p(�',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�rN���M��[Y���e',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�fˬZsJ8�i�HR�-&',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('PB)�N+��Ȫ��h�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�J�J��O��+՟A�D�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�[���NJ:�->�u�`',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('d(7�2C
��;s���',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�����@=� ����B�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('���*O̷�''���',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('@�����L���EKې��',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�����dC,���6+���',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('!����Dr�#~�e|',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�V���aB@�%����',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('I�EM��H���]}�WG�',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('F�Xg$}C��2Ƥ,���',NULL,NULL,NULL,50,'Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('=�ڏ�B[���m>�',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('z����J��g�OD�`',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES (X'0009b8fd05614d5cb64b3365e2b2e48d',NULL,NULL,NULL,'50,Verdad','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('f02��F��L�_��',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES (':�.��FѡE�N��',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('��(��C����7���',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('�e�2��IX����U�',NULL,NULL,NULL,'50,Mentira','Me gusta');
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('n���"C��l̡�؎�',NULL,NULL,NULL,NULL,NULL);
INSERT INTO "OpinionesUsuarios" ("IdOpinion","IdCategoriaTema","IdSubcategoriaTema","IdTema","ValoracionTema","opinionGeneral") VALUES ('_����fK䯤��c���',NULL,NULL,NULL,'50,Verdad','Me gusta');
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (1,'No me gusta nada',5);
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (2,'Decepcionante',6);
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (3,'Vergonzoso',7);
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (4,'Indiferente',3);
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (5,'Me encanta',1);
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (6,'Me gusta',2);
INSERT INTO "options_OpinionGeneral" ("IdOption_OpinionGeneral","Opcion","orden") VALUES (7,'No me gusta',4);
COMMIT;
