/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.log.dao;


import java.util.Date;
import java.util.UUID;
import peop.libs.log.core.LogCore.logStatus;

/**
 *
 * @author alquimista
 */
public class log {
    
    
    public UUID guid;
    public String Titulo;
    public String Descripcion;
    public String LogType;
    public Date DbTimeStamp;
    public logStatus Status;

    public log() {

    }

    public log(UUID guid, String Titulo, String Descripcion, String LogType, Date DbTimeStamp) {
        this.guid = guid;
        this.Titulo = Titulo;
        this.Descripcion = Descripcion;
        this.LogType = LogType;
        this.DbTimeStamp = DbTimeStamp;
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getLogType() {
        return LogType;
    }

    public void setLogType(String LogType) {
        this.LogType = LogType;
    }

    public Date getDbTimeStamp() {
        return DbTimeStamp;
    }

    public void setDbTimeStamp(Date DbTimeStamp) {
        this.DbTimeStamp = DbTimeStamp;
    }

    
    
}
