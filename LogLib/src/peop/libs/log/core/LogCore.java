/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peop.libs.log.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import peop.libs.log.dao.log;
import peop.libs.poolconnection.PoolConnectionCore;
import peop.libs.poolconnection.PoolConnectionCore.tipoConex;

/**
 *
 * @author alquimista
 */
public class LogCore {

    public static final PoolConnectionCore pcc = new PoolConnectionCore();

    public enum logStatus
    {        
        INFO,
        WARNING,
        ERROR
        
    }
    public void insertLog(log logUnit) {

        try {
            try (Connection conex = pcc.getConex(tipoConex.SQLite_log)) {
                Statement sentencia = conex.createStatement();

                if (logUnit.guid == null || "".equals(logUnit.guid.toString())) {
                    
                    logUnit.guid = UUID.randomUUID();
                }

                Date dtToday = Calendar.getInstance().getTime();
                
                

                String sql = "INSERT INTO ApplicationLog2020 (Id,titulo,descripcion,status,DbDateStamp,DbTimeStamp)VALUES('%s','%s','%s','INFO','%tD','%tT')";
                String queryInsert = String.format(sql, logUnit.guid,logUnit.Titulo,logUnit.Descripcion,dtToday, dtToday);

                int result = sentencia.executeUpdate(queryInsert);

                System.out.println("Insertado: " + result + " registro de log");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LogCore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
